package nl.aimsites.nzbvuq.gameview.model;

/**
 * @author Max Neerken (MD.Neerken@student.han.nl)
 */
public class LobbyNotFoundException extends RuntimeException {
}
