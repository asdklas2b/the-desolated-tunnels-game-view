package nl.aimsites.nzbvuq.gameview.model;

import nl.aimsites.nzbvuq.game.agentscriptconverter.IUseAgent;

/**
 * Model for InGameMenu.
 *
 * @author Max Neerken (MD.Neerken@student.han.nl)
 */
public class InGameMenuModel extends Model {

  // TODO move this logic to game
  private static final String PLAYER_NAME = "Jorrit";
  private static final String AGENT_NAME = "DefaultAgent";

  private final IUseAgent useAgent;
  private boolean agentOn;

  /**
   * Construct a new in-game menu model.
   *
   * @param useAgent interface used to control agent.
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public InGameMenuModel(IUseAgent useAgent) {
    this.useAgent = useAgent;
    agentOn = false;
  }

  /**
   * Lets the players software agent take control of the
   * player or if the agent is already on it turns the
   * agent off so the player has control.
   *
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public void swapAgent() {
    if (!agentOn) {
      useAgent.startUsingAgent(PLAYER_NAME, AGENT_NAME);
      agentOn = true;
    } else {
      useAgent.stopUsingAgent();
      agentOn = false;
    }
  }
}
