package nl.aimsites.nzbvuq.gameview.model;

import nl.aimsites.nzbvuq.game.agentscriptconverter.IAgentScriptConverter;
import nl.aimsites.nzbvuq.gameview.GameView;

/**
 * @author Ruben Eppink (RC.Eppink@student.han.nl)
 *
 * This class is responsible for calling the correct dependencies when
 * anything with game logic has to happen.
 *
 * */
public class AgentConfigurationModel extends Model {
  private final IAgentScriptConverter agentScriptConverter;

  /**
   * Constructs a new model and sets the dependencies
   *
   * @author Ruben Eppink (RC.Eppink@student.han.nl)
   */
  public AgentConfigurationModel() {
    agentScriptConverter = GameView.getAgentScriptConverter();
  }

  /**
   * Calls the agent script converter with the player input to change the behavior of an agent
   *
   * @author Ruben Eppink (RC.Eppink@student.han.nl)
   * @param input the player input
   */
  public void convert(String input) {
    agentScriptConverter.convert(input);
  }
}
