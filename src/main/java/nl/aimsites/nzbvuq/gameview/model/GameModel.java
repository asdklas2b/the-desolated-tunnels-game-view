package nl.aimsites.nzbvuq.gameview.model;

import nl.aimsites.nzbvuq.chat.enums.ChatType;
import nl.aimsites.nzbvuq.chat.interfaces.IDisplayChat;
import nl.aimsites.nzbvuq.game.inputhandler.parser.IInputHandler;
import nl.aimsites.nzbvuq.gameview.GameView;
import nl.aimsites.nzbvuq.game.inputhandler.PrintType;
import nl.aimsites.nzbvuq.gameview.Message;
import nl.aimsites.nzbvuq.gameview.contracts.provided.IDisplayGame;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Model for Game screen.
 *
 * @author Ruben Eppink (Ruben_eppink@Live.nl)
 * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
 */
public class GameModel extends Model implements IDisplayGame, IDisplayChat, IObservable<Message> {
  private static GameModel instance;
  protected final List<IObserver<Message>> observers = new ArrayList<>();
  private final IInputHandler inputHandler;

  private GameModel() {
    this.inputHandler = GameView.getInputHandler();
  }

  public static GameModel getInstance() {
    if (instance == null) {
      instance = new GameModel();
    }
    return instance;
  }

  @Override
  public void displayGameMessage(String gameMessage) {
    notifyObservers(new Message(gameMessage, false));
  }

  @Override
  public void addObserver(IObserver<Message> observer) {
    observers.add(observer);
  }

  @Override
  public void removeObserver(IObserver<Message> observer) {
    observers.remove(observer);
  }

  @Override
  public void notifyObservers(Message message) {
    observers.forEach(observer -> observer.update(message));
  }

  /**
   * Calls input handler to handle the players command
   *
   * @param playerInput command that the player sent to the game
   * @author Ruben Eppink (RC.Eppink@student.han.nl)
   */
  public void handleInput(String playerInput) {
    Scanner scanner = new Scanner(playerInput);
    inputHandler.handleInput(scanner);
  }

  @Override
  public void displayChatMessage(String chatMessage, String sender, ChatType chatType) {
    notifyObservers(new Message(chatMessage, true, sender, PrintType.DEFAULT, chatType));
  }

  @Override
  public void displayChatHistory(List<nl.aimsites.nzbvuq.chat.dto.Message> chatHistory) {
  }
}
