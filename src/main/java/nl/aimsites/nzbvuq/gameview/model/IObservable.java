package nl.aimsites.nzbvuq.gameview.model;

/**
 * This interface allows an object to be listened by observers.
 *
 * @author Max Neerken (MD.Neerken@student.han.nl)
 * @author Ruben Eppink (RC.Eppink@student.han.nl)
 */
public interface IObservable<T> {
  /**
   * Adds observer to the list of observers that are subscribed to the observable
   *
   * @param observer The observer that needs to be added to the list
   * @author Ruben Eppink & Max Neerken (RC.Eppink@student.han.nl & m.neerken@student.han.nl)
   */
  void addObserver(IObserver<T> observer);

  /**
   * Removes observer from the list of observers that are subscribed to the observable
   *
   * @param observer The observer that needs to be removed from the list
   * @author Ruben Eppink & Max Neerken (RC.Eppink@student.han.nl & m.neerken@student.han.nl)
   */
  void removeObserver(IObserver<T> observer);

  /**
   * Notifies all observers that are subscribed to the observable when the observable has a state
   * change
   *
   * @param changedObject object that had a state change and needs to be pushed to the observer
   * @author Ruben Eppink & Max Neerken (RC.Eppink@student.han.nl & m.neerken@student.han.nl)
   */
  void notifyObservers(T changedObject);
}
