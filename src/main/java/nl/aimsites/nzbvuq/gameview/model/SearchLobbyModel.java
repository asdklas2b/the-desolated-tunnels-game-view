package nl.aimsites.nzbvuq.gameview.model;

import lombok.Setter;
import nl.aimsites.nzbvuq.gameview.controller.SearchLobbyController;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyListenerContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyManagerContract;

import java.util.ArrayList;
import java.util.List;

/**
 * Model for SearchLobby screen.
 *
 * @author Max Neerken (MD.Neerken@student.han.nl)
 */
public class SearchLobbyModel extends Model implements IObservable<SearchLobbyModel>, LobbyListenerContract {

  private final LobbyManagerContract lobbyManager;
  protected List<IObserver<SearchLobbyModel>> observers;
  @Setter
  private SearchLobbyController controller;

  /**
   * Creates a new SearchLobbyModel and listens for
   * updates in lobbies.
   *
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public SearchLobbyModel(LobbyManagerContract lobbyManager) {
    observers = new ArrayList<>();
    this.lobbyManager = lobbyManager;
    this.lobbyManager.listen(this);
  }

  /**
   * Retrieves lobbies with the indexes between the given two.
   *
   * @return A list of lobbies.
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public List<LobbyContract> getLobbies(int lowerIndex, int upperIndex) {
    return lobbyManager.getLobbies().subList(lowerIndex, upperIndex);
  }

  /**
   * Returns the total amount of lobbies.
   *
   * @return The total amount of lobbies
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public int getLobbyCount() {
    return lobbyManager.getLobbies().size();
  }

  /**
   * Returns a specific lobby.
   *
   * @param index The index of the lobby on the current page.
   * @return The lobbyDTO of the lobby.
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public LobbyContract getLobby(int index) {
    return lobbyManager.getLobbies().get(index);
  }

  /**
   * This method lets the player join a lobby.
   *
   * @param lobby The lobby that the player wants to join.
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public void joinLobby(LobbyContract lobby) {
    lobbyManager.join(lobby);
  }

  @Override
  public void addObserver(IObserver<SearchLobbyModel> observer) {
    observers.add(observer);
  }

  @Override
  public void removeObserver(IObserver<SearchLobbyModel> observer) {
    observers.remove(observer);
  }

  @Override
  public void notifyObservers(SearchLobbyModel changedObject) {
    observers.forEach(observer -> observer.update(changedObject));
  }

  @Override
  public void onAdd(LobbyContract lobby) {
    notifyObservers(this);
  }

  @Override
  public void onUpdate(LobbyContract lobby) {
    notifyObservers(this);
  }

  @Override
  public void onRemove(LobbyContract lobby) {
    notifyObservers(this);
  }

  @Override
  public void onStartGame(LobbyContract lobby) {
    notifyObservers(this);
  }

  @Override
  public void onJoin(LobbyContract lobby) {
    controller.joinGame();
  }
}
