package nl.aimsites.nzbvuq.gameview.model;

/**
 * This abstract class can be extended to mark a class as
 * model class so it can be used by a controller.
 *
 * @author Ruben Eppink (RC.Eppink@student.han.nl)
 */
public abstract class Model {

}
