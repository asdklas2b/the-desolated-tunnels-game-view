package nl.aimsites.nzbvuq.gameview.model;

import lombok.Setter;
import nl.aimsites.nzbvuq.gameview.controller.LobbyController;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyListenerContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyManagerContract;
import nl.aimsites.nzbvuq.lobby.exceptions.LobbyException;

import java.util.ArrayList;
import java.util.List;

/**
 * Model for lobby screen.
 *
 * @author Max Neerken (MD.Neerken@student.han.nl)
 */
public class LobbyModel extends Model implements IObservable<LobbyModel>, LobbyListenerContract {

  private final LobbyManagerContract lobbyManager;
  private final String lobbyID;
  protected List<IObserver<LobbyModel>> observers;

  @Setter
  private LobbyController controller;

  /**
   * Creates a new LobbyModel.
   *
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public LobbyModel(LobbyManagerContract lobbyManager) {
    observers = new ArrayList<>();
    this.lobbyManager = lobbyManager;
    this.lobbyManager.listen(this);

    lobbyID = getLobby().getIdentifier();
  }


  /**
   * Returns true if the player is the owner of
   * a lobby.
   *
   * @return Is the player the owner of a lobby true or false.
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public boolean isLobbyOwner() {
    return lobbyManager.getOwnLobby() != null;
  }

  /**
   * Starts the game.
   *
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public void startGame() {
    try {
      lobbyManager.startGame();
    } catch (LobbyException e) {
      throw new LobbyNotFoundException();
    }
  }

  /**
   * Returns the lobby the player is currently in.
   *
   * @return The lobby the player is currently in.
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public LobbyContract getLobby() {
    var joinedLobby = lobbyManager.getJoinedLobby();

    if (joinedLobby == null)
      throw new LobbyNotFoundException();

    return joinedLobby;
  }

  /**
   * This method lets the player leave his current lobby or
   * disband it, if player is the lobby creator.
   *
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public void leaveLobby() {
    if (lobbyManager.getOwnLobby() != null) {
      lobbyManager.disband();
    } else {
      lobbyManager.leave();
    }
  }

  @Override
  public void addObserver(IObserver<LobbyModel> observer) {
    observers.add(observer);
  }

  @Override
  public void removeObserver(IObserver<LobbyModel> observer) {
    observers.remove(observer);
  }

  @Override
  public void notifyObservers(LobbyModel changedObject) {
    observers.forEach(observer -> observer.update(this));
  }

  @Override
  public void onAdd(LobbyContract lobby) {
    if (lobby.getIdentifier().equals(lobbyID)) {
      notifyObservers(this);
    }
  }

  @Override
  public void onUpdate(LobbyContract lobby) {
    if (lobby.getIdentifier().equals(lobbyID)) {
      notifyObservers(this);
    }
  }

  @Override
  public void onRemove(LobbyContract lobby) {
    if (lobby.getIdentifier().equals(lobbyID)) {
      notifyObservers(this);
    }
  }

  @Override
  public void onStartGame(LobbyContract lobby) {
    if (lobby.getIdentifier().equals(lobbyID)) {
      controller.startGame();
    }
  }

  @Override
  public void onJoin(LobbyContract lobby) {
    if (lobby.getIdentifier().equals(lobbyID)) {
      notifyObservers(this);
    }
  }
}
