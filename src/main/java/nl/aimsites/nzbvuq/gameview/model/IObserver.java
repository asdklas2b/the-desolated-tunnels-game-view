package nl.aimsites.nzbvuq.gameview.model;

/**
 * This interface allows an object to listen another object.
 *
 * @author Max Neerken (MD.Neerken@student.han.nl)
 * @author Ruben Eppink (RC.Eppink@student.han.nl)
 */
public interface IObserver<T> {

  /**
   * Is called when the observable has a state change. The observer can update accordingly to the
   * state change.
   *
   * @param changedObject object that had a state change and was pushed from the observable
   * @author Ruben Eppink & Max Neerken (RC.Eppink@student.han.nl & m.neerken@student.han.nl)
   */
  void update(T changedObject);
}
