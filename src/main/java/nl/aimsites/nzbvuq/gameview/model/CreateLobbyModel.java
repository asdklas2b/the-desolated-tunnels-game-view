package nl.aimsites.nzbvuq.gameview.model;

import lombok.Setter;
import nl.aimsites.nzbvuq.gameview.GameView;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyManagerContract;

public class CreateLobbyModel extends Model {
  @Setter
  private LobbyManagerContract lobbyManager;

  public CreateLobbyModel() {
    lobbyManager = GameView.getLobbyManager();
  }

  /**
   * Sends the lobby information to the create Lobby function in the Lobby interface.
   *
   * @param lobbyName          The name of the lobby.
   * @param maxAmountOfPlayers The maximum amount of players.
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public void createLobby(String lobbyName, int maxAmountOfPlayers) {
    lobbyManager.create(lobbyName, maxAmountOfPlayers);
  }

  /**
   * Validates if the maximum amount of players is a valid value between or equal to 1 and 20.
   *
   * @param maxAmountOfPlayers The maximum amount of players.
   * @return boolean true or false depending on the given maximum amount of players.
   * @author Tim Weening (T.Weening@student.han.nl) & Ruben Eppink (RC.Eppink@student.han.nl)
   */
  public boolean checkMaxAmountOfPlayersValid(int maxAmountOfPlayers) {
    return maxAmountOfPlayers >= 1 && maxAmountOfPlayers <= 20;
  }
}
