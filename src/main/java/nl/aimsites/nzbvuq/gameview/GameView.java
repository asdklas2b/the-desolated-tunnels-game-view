package nl.aimsites.nzbvuq.gameview;

import com.valkryst.VTerminal.Screen;
import com.valkryst.VTerminal.font.Font;
import com.valkryst.VTerminal.font.FontLoader;
import lombok.Getter;
import nl.aimsites.nzbvuq.game.agentscriptconverter.IUseAgent;
import nl.aimsites.nzbvuq.chat.interfaces.ISendMessage;
import nl.aimsites.nzbvuq.game.agentscriptconverter.IAgentScriptConverter;
import nl.aimsites.nzbvuq.game.inputhandler.parser.IInputHandler;
import nl.aimsites.nzbvuq.gameview.controller.GameController;
import nl.aimsites.nzbvuq.gameview.controller.MainMenuController;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyManagerContract;

import java.io.IOException;
import java.util.Objects;

/**
 * This class starts the view component.
 *
 * @author Max Neerken (MD.Neerken@student.han.nl)
 * @author Joost Lawerman <joostlawerman@users.noreply.github.com>
 */
public class GameView {

  private Screen screen;

  @Getter
  private static LobbyManagerContract lobbyManager;

  @Getter
  private static ISendMessage sendMessage;

  @Getter
  private static IInputHandler inputHandler;

  @Getter
  private static IUseAgent useAgent;

  @Getter
  private static IAgentScriptConverter agentScriptConverter;

  /**
   * Create new GameView instance
   *
   * @param lobbyManager The lobbyManager that gives access to the models lobby logic.
   * @param sendMessage  The sendMessage instance that is used to send messages to other players.
   * @param inputHandler The inputHandler that is used to process user actions while playing.
   * @param agentScriptConverter Converts user input to rules for his/her agent.
   *
   * @author Joost Lawerman <joostlawerman@users.noreply.github.com>
   */
  public GameView(LobbyManagerContract lobbyManager, ISendMessage sendMessage, IInputHandler inputHandler, IAgentScriptConverter agentScriptConverter, IUseAgent useAgent) {
    GameView.agentScriptConverter = agentScriptConverter;
    GameView.lobbyManager = lobbyManager;
    GameView.sendMessage = sendMessage;
    GameView.inputHandler = inputHandler;
    GameView.useAgent = useAgent;
  }

  /**
   * Init basic
   *
   * @author Joost Lawerman <joostlawerman@users.noreply.github.com>
   */
  public void initialize() {
    if (screen == null) {
      Font font;

      try {
        font = FontLoader.loadFont(Objects.requireNonNull(this.getClass().getClassLoader()
            .getResourceAsStream("Fonts/DejaVu Sans Mono/18pt/bitmap.png")),
          Objects.requireNonNull(this.getClass().getClassLoader()
            .getResourceAsStream("Fonts/DejaVu Sans Mono/18pt/data.fnt")), 1);
        screen = new Screen(80, 40, font);
      } catch (IOException e) {
        throw new FontNotFoundException(e);
      }

      screen.addCanvasToFrame();
    }
  }

  /**
   * Starts the view component. When called a loop will be started on a new thread
   * that keeps the frontend running.
   *
   * @author Max Neerken (MD.Neerken@student.han.nl) Joost Lawerman (JJ.Lawerman@student.han.nl) & Tom Esendam (TS.Essendam@student.han.nl)
   */
  public MainMenuController start() {
    initialize();

    return new MainMenuController(screen);
  }

  /**
   * Starts from the game view controller.
   *
   * @author Joost Lawerman <joostlawerman@users.noreply.github.com>
   */
  public void startFromGameController() {
    initialize();

    GameController.getInstance(screen, start());
  }
}
