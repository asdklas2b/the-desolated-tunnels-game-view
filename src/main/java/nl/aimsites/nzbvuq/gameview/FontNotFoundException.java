package nl.aimsites.nzbvuq.gameview;

import java.io.IOException;

/**
 * @author Max Neerken (MD.Neerken@student.han.nl)
 */
public class FontNotFoundException extends RuntimeException {
  FontNotFoundException(IOException e) {
    super(e);
  }
}
