package nl.aimsites.nzbvuq.gameview.contracts.provided;

public interface IDisplayGame {

  /**
   * Displays a game message on the screen
   *
   * @param gameMessage The game message that needs to be displayed
   * @author Ruben Eppink (RC.Eppink@student.han.nl)
   */
  void displayGameMessage(String gameMessage);
}
