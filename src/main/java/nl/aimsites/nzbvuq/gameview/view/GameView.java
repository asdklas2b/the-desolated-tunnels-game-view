package nl.aimsites.nzbvuq.gameview.view;

import com.valkryst.VTerminal.Screen;
import com.valkryst.VTerminal.builder.LabelBuilder;
import com.valkryst.VTerminal.component.Button;
import com.valkryst.VTerminal.component.Component;
import com.valkryst.VTerminal.component.Label;
import com.valkryst.VTerminal.component.TextArea;
import com.valkryst.VTerminal.palette.java2d.Java2DPalette;
import lombok.Getter;
import nl.aimsites.nzbvuq.game.inputhandler.PrintType;
import nl.aimsites.nzbvuq.game.inputhandler.Printer;
import nl.aimsites.nzbvuq.gameview.Message;
import nl.aimsites.nzbvuq.gameview.model.IObserver;
import org.apache.commons.lang3.StringUtils;

import java.awt.*;
import java.io.IOException;
import java.util.List;
import java.util.*;

/**
 * @author Ruben Eppink (RC.Eppink@student.han.nl)
 *
 * This class is responsible for the visual representation of the
 * game screen
 *
 * */
public class GameView extends View implements IObserver<Message>, Printer {
  private static final int GAME_AREA_WIDTH = 60;
  private static final int CHAT_AREA_WIDTH = 20;

  @Getter private TextArea chatArea;
  private static final int GAME_AREA_HEIGHT = 36;

  @Getter private TextArea gameArea;

  @Getter private Button buttonPause;


  private final Deque<LabelWrapper> view = new ArrayDeque(GAME_AREA_HEIGHT);

  private static Java2DPalette inputPalette;
  private static Java2DPalette outputPalette;

  private static Java2DPalette getInputPalette() {
    if (inputPalette == null) {
      try {
        inputPalette = new Java2DPalette("/Palettes/Input.json");
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
      inputPalette.setDefaultForeground(
        new Color(255, 255, 255).getRGB());
    }
    return inputPalette;
  }

  private static Java2DPalette getOutputPalette() {
    if (outputPalette == null) {
      try {
        outputPalette = new Java2DPalette("/Palettes/Output.json");
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
      outputPalette.setDefaultForeground(
        new Color(255, 0, 0).getRGB());
    }
    return outputPalette;
  }

  public class LabelWrapper {
    @Getter
    private final String text;

    @Getter
    private Label label;

    @Getter
    private PrintType input;

    public LabelWrapper(String text, int x, int y, PrintType input) {
      this.text = text;
      this.input = input;

      move(
        x,
        y
      );
    }

    public Label move(int x, int y) {
      var builder = new LabelBuilder();

      builder.setText(text);
      builder.setPosition(x, y);

      switch (input) {
        case INPUT -> builder.setPalette(getInputPalette());
        case OUTPUT -> builder.setPalette(getOutputPalette());
      }

      label = builder.build();

      return label;
    }
  }

  /**
   * Constructs a new View that fills the entire screen.
   *
   * @param screen The screen on which the view is displayed.
   */
  public GameView(Screen screen) {
    super(screen);
  }

  @Override
  protected List<Component> initializeComponents() {
    List<Component> components = new ArrayList<>();

    buttonPause = createButton("ESC. Pauzemenu", 0, 0);
    components.add(buttonPause);

    userInputArea = createTextArea(0, 38, 80, 2);
    components.add(userInputArea);

    chatArea = createTextArea(60, 1, CHAT_AREA_WIDTH, 37);
    chatArea.setEditable(false);
    chatArea.changeCaretPosition(-1, -1);
    chatArea.setBackgroundColor(new Color(40, 40, 40));
    components.add(chatArea);

    return components;
  }

  @Override
  public void update(Message message) {
    if (message.isChatMessage()) {
      Message.DEFAULT_BACKGROUND_COLOR = chatArea.getBackgroundColor();
      formatMessage(message, CHAT_AREA_WIDTH)
        .forEach(message1 -> chatArea.appendText(message1.getMessage()));
    } else {
      addGameMessage(
        message
      );
    }
  }

  @Override
  public void println(String line, PrintType type) {
    addGameMessage(
      new Message(
        line,
        false,
        type
      )
    );
  }

  public void addGameMessage(Message message) {
    var rows = Arrays.stream(message.getTextMessage().split("\n"))
      .map(
        row -> indentString(row, 4).trim()
      )
      .flatMap(
        row -> {
          if (row.length() > GAME_AREA_WIDTH) {
            return Arrays.stream(new String[]{
              row.substring(0, GAME_AREA_WIDTH),
              StringUtils.repeat(" ", 4) + row.substring(GAME_AREA_WIDTH).trim()
            }.clone());
          }
          return Arrays.stream(new String[]{
            row
          }.clone());
        }
      )
      .toArray(String[]::new);

    if (view.size() + rows.length >= GAME_AREA_HEIGHT) {
      for (int i = 0; i < rows.length; i++) {
        SCREEN.removeComponent(
          view.removeFirst()
            .getLabel()
        );
      }
    }

    var i = 1;
    for (LabelWrapper label : view) {
      SCREEN.removeComponent(
        label.getLabel()
      );

      SCREEN.addComponent(
        label.move(
          0,
          i++
        )
      );
    }

    for (var line : rows) {
      var label = new LabelWrapper(
        (message.getOutput() == PrintType.INPUT ? "-> " : "" ) + line,
        0,
        view.size() + 1,
        message.getOutput()
      );

      view.add(label);

      SCREEN.addComponent(label.getLabel());
    }

    SCREEN.draw();
  }

  public String indentString(String str, int indentation) {
    var out = new StringBuilder();
    for (var peice : str.split("\t")) {
      out.append(peice);
      out.append(
        StringUtils.repeat(
          " ",
          peice.length() < 1 ?
            indentation :
            (indentation - (peice.length() % indentation))
        )
      );
    }
    return out.toString();
  }

  /**
   * Formats message for reading in UI.
   *
   * @param message content of the message
   * @param textAreaWidth width of text
   * @return list which contains values needed to show message on UI
   *
   * @author RC.Eppink@student.han.nl
   * @author JA.vanVugt@student.han.nl
   */
  private List<Message> formatMessage(Message message, int textAreaWidth) {
    List<Message> messages = new ArrayList<>();
    int messageLength = message.getTextMessage().length();
    int nrOfRows = (int) Math.ceil(messageLength / (float) textAreaWidth);

    if (messageLength > textAreaWidth) {
      for (int i = 0; i < nrOfRows; i++) {
        messages.add(
          new Message(
            message
              .getTextMessage()
              .substring(i * textAreaWidth, Math.max((i + textAreaWidth), messageLength)),
            message.isChatMessage()));
      }
    } else {
      messages.add(message);
    }

    return messages;
  }
}
