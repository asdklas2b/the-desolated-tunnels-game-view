package nl.aimsites.nzbvuq.gameview.view;

import com.valkryst.VTerminal.Screen;
import com.valkryst.VTerminal.component.Button;
import com.valkryst.VTerminal.component.Component;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Ruben Eppink (RC.Eppink@student.han.nl)
 *
 * This class is responsible for the visual representation of the
 * agent configuration screen where the player can edit the behavior
 * of their agent(s)
 *
 * */
public class AgentConfigurationView extends View {
  @Getter private Button buttonBack;

  /**
   * Constructs a new View that fills the entire screen.
   *
   * @param screen The screen on which the view is displayed.
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public AgentConfigurationView(Screen screen) {
    super(screen);
  }

  @Override
  protected List<Component> initializeComponents() {
    var components = new ArrayList<Component>();

    components.add(createLabel("Agentconfiguratie", 2, 2));

    components.add(createLabel("Dit zijn de standaard agent instellingen: ", 2, 4));

    components.add(createLabel(" - Set main priority, explore, kill enemy.", 2, 5));

    components.add(createLabel(" - Set inventory priority, 6 food, 3 armor, 1 weapon", 2, 6));

    components.add(createLabel(" - Use food if strength < 400.", 2, 7));

    components.add(createLabel(" - Use best weapon as default.", 2, 8));

    components.add(
        createLabel(
            " - When seeing monster fight if monster strength less than 500 otherwise flee.",
            2,
            9));

    components.add(
        createLabel(
            " - When seeing enemy fight if enemy strength less than 500 otherwise flee.", 2, 10));

    components.add(createLabel(" - When seeing attribute pick it up.", 2, 11));

    components.add(
        createLabel(" - When seeing weapon replace if current weapon has lower strength.", 2, 12));

    components.add(createLabel("Stel je eigen agent in door gedragsregels onder in de balk te typen.", 2, 15));

    buttonBack = createButton("0. Terug", 5, 18);
    components.add(buttonBack);

    components.add(createLabel("Maak een keuze (0, 0, 0 of 0)", 1, 21));

    userInputArea = createTextArea(0, 22, 80, 18);
    components.add(userInputArea);
    userInputArea.changeCaretPosition(0,0);

    return components;
  }
}
