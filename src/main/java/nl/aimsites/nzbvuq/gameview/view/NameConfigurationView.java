package nl.aimsites.nzbvuq.gameview.view;

import com.valkryst.VTerminal.Screen;
import com.valkryst.VTerminal.component.Button;
import com.valkryst.VTerminal.component.Component;
import com.valkryst.VTerminal.component.Label;
import com.valkryst.VTerminal.component.TextArea;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public class NameConfigurationView extends View {
  @Getter
  private Button buttonBack;

  @Getter
  private Button buttonSave;

  @Getter
  private TextArea userInputName;

  /**
   * Constructs a new View that fills the entire screen.
   *
   * @param screen The screen on which the view is displayed.
   */
  public NameConfigurationView(Screen screen) {
    super(screen);
  }

  @Override
  protected List<Component> initializeComponents() {
    var components = new ArrayList<Component>();

    components.add(createLabel("Configureren naam", 5, 3));

    components.add(createLabel("Voer hieronder je spelersnaam in.", 5, 6));

    components.add(createLabel("Username:", 5, 18));
    userInputName = createTextArea(30, 18, 40, 1);
    components.add(userInputName);

    buttonBack = createButton("0. Terug", 5, 34);
    components.add(buttonBack);

    buttonSave = createButton("1. Opslaan", 30, 34);
    components.add(buttonSave);

    components.add(createLabel("Maak een keuze: 1 of 0", 0, 37));

    userInputArea = createTextArea(0, 38, 80, 2);
    components.add(userInputArea);

    return components;
  }
}
