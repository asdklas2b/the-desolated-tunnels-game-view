package nl.aimsites.nzbvuq.gameview.view;

import com.valkryst.VTerminal.Screen;
import com.valkryst.VTerminal.builder.ButtonBuilder;
import com.valkryst.VTerminal.builder.LabelBuilder;
import com.valkryst.VTerminal.builder.TextAreaBuilder;
import com.valkryst.VTerminal.component.Button;
import com.valkryst.VTerminal.component.Component;
import com.valkryst.VTerminal.component.Label;
import com.valkryst.VTerminal.component.TextArea;
import com.valkryst.VTerminal.component.*;
import lombok.Getter;
import lombok.NonNull;

import java.awt.*;
import java.util.List;

public abstract class View extends Layer {

  protected final Screen SCREEN;

  /**
   * The text area in which the user can enter his input.
   */
  @Getter
  protected TextArea userInputArea;

  /**
   * Used to create buttons.
   */
  private ButtonBuilder buttonBuilder;
  /**
   * Used to create text areas.
   */
  private TextAreaBuilder textAreaBuilder;
  /**
   * Used to create labels
   */
  private LabelBuilder labelBuilder;

  /**
   * Constructs a new View that fills the entire screen.
   *
   * @param screen The screen on which the view is displayed.
   * @throws NullPointerException If the screen is null.
   */
  public View(final @NonNull Screen screen) {
    super(new Dimension(screen.getWidth(), screen.getHeight()));
    this.SCREEN = screen;
    addComponents(initializeComponents());
  }

  /**
   * Creates a button that is to be displayed on the screen
   *
   * @param text The text that is to be displayed on the button.
   * @param x    The horizontal position of the button on the screen.
   * @param y    The vertical position of the button on the screen.
   * @return The created button.
   */
  protected Button createButton(String text, int x, int y) {
    if (buttonBuilder == null)
      buttonBuilder = new ButtonBuilder();

    buttonBuilder.setText(text);
    buttonBuilder.setPosition(x, y);
    return buttonBuilder.build();
  }

  protected TextArea createTextArea(int x, int y, int width, int height) {
    if (textAreaBuilder == null)
      textAreaBuilder = new TextAreaBuilder();

    textAreaBuilder.setWidth(width);
    textAreaBuilder.setHeight(height);
    textAreaBuilder.setPosition(x, y);
    return textAreaBuilder.build();
  }

  protected Label createLabel(String text, int x, int y) {
    if (labelBuilder == null)
      labelBuilder = new LabelBuilder();

    labelBuilder.setText(text);
    labelBuilder.setPosition(x, y);
    return labelBuilder.build();
  }

  /**
   * Used to create components to be displayed on the screen.
   *
   * @return A list containing the created components.
   */
  protected abstract List<Component> initializeComponents();

  protected <T extends Component> void addComponents(List<T> components) {
    if (components == null)
      return;

    components.forEach(this::addComponent);

    SCREEN.addComponent(this);
  }
}
