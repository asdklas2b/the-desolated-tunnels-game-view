package nl.aimsites.nzbvuq.gameview.view;

import com.valkryst.VTerminal.Screen;
import com.valkryst.VTerminal.component.Button;
import com.valkryst.VTerminal.component.Component;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * View for InGameMenu.
 *
 * @author Tim Weening (T.Weening@student.han.nl)
 */
public class InGameMenuView extends View {
  @Getter
  private Button buttonSwapAgent;
  @Getter
  private Button buttonConfiguration;
  @Getter
  private Button buttonSaveAndExit;
  @Getter
  private Button buttonResumeGame;

  /**
   * Constructs a new View that fills the entire screen.
   *
   * @param screen The screen on which the view is displayed.
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public InGameMenuView(Screen screen) {
    super(screen);
  }

  @Override
  protected List<Component> initializeComponents() {
    SCREEN.draw();

    var components = new ArrayList<Component>();

    components.add(createLabel("Pauzemenu", 5, 5));

    buttonSwapAgent = createButton("1. Agent wisselen", 5, 28);
    components.add(buttonSwapAgent);

    buttonConfiguration = createButton("2. Configuratiemenu", 5, 30);
    components.add(buttonConfiguration);

    buttonSaveAndExit = createButton("3. Speelsessie opslaan en stoppen", 5, 32);
    components.add(buttonSaveAndExit);

    buttonResumeGame = createButton("0. Terug naar spel", 5, 34);
    components.add(buttonResumeGame);

    components.add(createLabel("Maak een keuze (1, 2, 3 of 0)", 1, 37));

    userInputArea = createTextArea(0, 38, 80, 2);
    components.add(userInputArea);

    return components;
  }

}
