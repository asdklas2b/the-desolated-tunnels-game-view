package nl.aimsites.nzbvuq.gameview.view;

import com.valkryst.VTerminal.Screen;
import com.valkryst.VTerminal.component.Button;
import com.valkryst.VTerminal.component.Component;
import com.valkryst.VTerminal.component.Layer;
import com.valkryst.VTerminal.printer.ImagePrinter;
import lombok.Getter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainMenuView extends View {
  @Getter
  private Button buttonCreateLobby;
  @Getter
  private Button buttonSearchLobby;
  @Getter
  private Button buttonConfiguration;
  @Getter
  private Button buttonExit;

  /**
   * Constructs a new View that fills the entire screen.
   *
   * @param screen The screen on which the view is displayed.
   */
  public MainMenuView(Screen screen) {
    super(screen);
  }

  @Override
  protected List<Component> initializeComponents() {
    var components = new ArrayList<Component>();

    components.add(loadLogo());

    buttonCreateLobby = createButton("1. Lobby aanmaken", 5, 20);
    components.add(buttonCreateLobby);

    buttonSearchLobby = createButton("2. Lobby zoeken", 5, 22);
    components.add(buttonSearchLobby);

    buttonConfiguration = createButton("3. Configuratiemenu", 5, 24);
    components.add(buttonConfiguration);

    buttonExit = createButton("0. Afsluiten", 5, 26);
    components.add(buttonExit);

    components.add(createLabel("Maak een keuze (1, 2, 3 of 0)", 1, 37));

    userInputArea = createTextArea(0, 38, 80, 2);
    components.add(userInputArea);


    return components;
  }


  private Layer loadLogo() {
    final var layer = new Layer(new Dimension(80, 16), new Point(0, 0));

    final var thread = Thread.currentThread();
    final var classLoader = thread.getContextClassLoader();

    try (final var is = classLoader.getResourceAsStream("afbeelding1.png")) {
      final var image = ImageIO.read(is);

      final var printer = new ImagePrinter(image);
      printer.printDetailed(layer.getTiles(), new Point(0, 0));
    } catch (final IOException | IllegalArgumentException e) {
      e.printStackTrace();
    }

    return layer;
  }
}
