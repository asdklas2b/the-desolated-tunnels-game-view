package nl.aimsites.nzbvuq.gameview.view;

import com.valkryst.VTerminal.Screen;
import com.valkryst.VTerminal.component.Button;
import com.valkryst.VTerminal.component.Component;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class GameSettingsView extends View {
  @Getter
  private Button buttonGameModes;
  @Getter
  private Button buttonAgents;
  @Getter
  private Button buttonMonsters;
  @Getter
  private Button buttonAttributes;
  @Getter
  private Button buttonConfiguration;
  @Getter
  private Button buttonResumeGame;
  @Getter
  private Button buttonBack;

  /**
   * Constructs a new View that fills the entire screen.
   *
   * @param screen The screen on which the view is displayed.
   */
  public GameSettingsView(Screen screen) {
    super(screen);
  }

  @Override
  protected List<Component> initializeComponents() {
    List<Component> components = new ArrayList<>();

    buttonGameModes = createButton("1. spelmodus", 3, 23);
    components.add(buttonGameModes);

    buttonAgents = createButton("2. agents", 3, 25);
    components.add(buttonAgents);

    buttonMonsters = createButton("3. monsters", 3, 27);
    components.add(buttonMonsters);

    buttonAttributes = createButton("4. attributen", 3, 29);
    components.add(buttonAttributes);

    buttonConfiguration = createButton("5. configuratie", 3, 31);
    components.add(buttonConfiguration);

    buttonResumeGame = createButton("6. spel hervatten", 3, 33);
    components.add(buttonResumeGame);

    buttonBack = createButton("0. terug", 3, 35);
    components.add(buttonBack);

    components.add(createLabel("Maak een keuze (1, 2, 3, 4, 5 of 6)", 1, 37));

    components.add(createLabel("Spelinstellingen", 1, 1));

    userInputArea = createTextArea(0, 38, 80, 2);
    components.add(userInputArea);

    return components;
  }
}
