package nl.aimsites.nzbvuq.gameview.view;

import com.valkryst.VTerminal.Screen;
import com.valkryst.VTerminal.component.Button;
import com.valkryst.VTerminal.component.Component;
import com.valkryst.VTerminal.component.Label;
import lombok.Getter;
import nl.aimsites.nzbvuq.gameview.model.IObserver;
import nl.aimsites.nzbvuq.gameview.model.LobbyModel;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyContract;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Max Neerken (MD.Neerken@student.han.nl)
 *
 * This class is responsible for the visual reprensentation of a specific lobby screen
 */
public class LobbyView extends View implements IObserver<LobbyModel> {

  @Getter private Button buttonStartGame;
  @Getter private Button buttonGameSettings;
  @Getter private Button buttonLeaveLobby;

  private List<Label> playerIndexLabels;
  private List<Label> playerLabels;

  private boolean named = false;

  /**
   * Constructs a new View that fills the entire screen.
   *
   * @param screen The screen on which the view is displayed.
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public LobbyView(Screen screen) {
    super(screen);
  }

  @Override
  protected List<Component> initializeComponents() {
    var components = new ArrayList<Component>();

    createLobbyOwnerComponents();
    components.add(createLabel("# | Speler                  |                  Agent |", 5, 7));

    buttonLeaveLobby = createButton("0. Spel verlaten", 2, 32);
    components.add(buttonLeaveLobby);

    components.add(createLabel("Kies een optie (1, 2 of 0)", 2, 36));

    userInputArea = createTextArea(0, 38, 80, 2);
    components.add(userInputArea);

    return components;
  }

  /**
   * Creates buttons that should only be shown if the player is the owner of the lobby.
   *
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public void createLobbyOwnerComponents() {
    buttonStartGame = createButton("1. Spel starten", 2, 30);
    buttonGameSettings = createButton("2. Spelinstellingen", 2, 31);
  }

  /**
   * Shows buttons that should only be shown if the player is the owner of the lobby.
   *
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public void showLobbyOwnerComponents() {
    addComponent(buttonStartGame);
    addComponent(buttonGameSettings);
  }

  /**
   * Displays lobby specific information on the screen.
   *
   * @param model The lobby model.
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public void displayLobbyData(LobbyModel model) {
    displayLobbyData(model.getLobby());
  }

  /**
   * Displays players in lobby.
   *
   * @param lobby The lobby which is shown.
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  private void displayLobbyData(LobbyContract lobby) {
    if (!named) {
      named = true;
      addComponents(
        List.of(createLabel("Lobby: " + lobby.getName(), 5, 5))
      );
    }

    removePlayerComponents();

    playerIndexLabels = new ArrayList<>();
    playerLabels = new ArrayList<>();

    for (var i = 0; i < lobby.getPlayersCount(); i++) {
      playerIndexLabels.add(createLabel(String.valueOf(i + 1).concat("."), 5, 10 + i));
      playerLabels.add(createLabel(lobby.getPlayers().get(i).getName(), 9, 10 + i));
    }

    addPlayerComponents();
  }

  /**
   * Removes any player labels.
   *
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  private void removePlayerComponents() {
    if (playerLabels != null) {
      playerIndexLabels.forEach(this::removeComponent);
      playerLabels.forEach(this::removeComponent);
    }
  }

  /**
   * Adds player labels to screen.
   *
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  private void addPlayerComponents() {
    addComponents(playerIndexLabels);
    addComponents(playerLabels);
  }

  @Override
  public void update(LobbyModel changedObject) {
    displayLobbyData(changedObject);
  }
}
