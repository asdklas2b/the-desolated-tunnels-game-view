package nl.aimsites.nzbvuq.gameview.view;

import com.valkryst.VTerminal.Screen;
import com.valkryst.VTerminal.component.Button;
import com.valkryst.VTerminal.component.Component;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class ConfigurationView extends View {

  @Getter
  private Button buttonAgents;
  @Getter
  private Button buttonMonsters;
  @Getter
  private Button buttonAttributes;
  @Getter
  private Button buttonName;
  @Getter
  private Button buttonBack;

  /**
   * Constructs a new View that fills the entire screen.
   *
   * @param screen The screen on which the view is displayed.
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public ConfigurationView(Screen screen) {
    super(screen);
  }

  @Override
  protected List<Component> initializeComponents() {
    var components = new ArrayList<Component>();

    components.add(createLabel("Configuratiemenu", 5, 5));

    buttonAgents = createButton("1. Agents configureren", 5, 26);
    components.add(buttonAgents);

    buttonMonsters = createButton("2. Monsters configureren", 5, 28);
    components.add(buttonMonsters);

    buttonAttributes = createButton("3. Attributen configureren", 5, 30);
    components.add(buttonAttributes);

    buttonName = createButton("4. Naam configureren", 5, 32);
    components.add(buttonName);

    buttonBack = createButton("0. Terug", 5, 34);
    components.add(buttonBack);

    components.add(createLabel("Maak een keuze (1, 2, 3, 4 of 0)", 1, 37));

    userInputArea = createTextArea(0, 38, 80, 2);
    components.add(userInputArea);

    return components;
  }
}
