package nl.aimsites.nzbvuq.gameview.view;

import com.valkryst.VTerminal.Screen;
import com.valkryst.VTerminal.component.Button;
import com.valkryst.VTerminal.component.Component;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class AttributeConfigurationView extends View {
  @Getter
  private Button buttonBack;

  /**
   * Constructs a new View that fills the entire screen.
   *
   * @param screen The screen on which the view is displayed.
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public AttributeConfigurationView(Screen screen) {
    super(screen);
  }

  @Override
  protected List<Component> initializeComponents() {
    var components = new ArrayList<Component>();

    components.add(createLabel("Attribuutconfiguratie", 5, 5));

    buttonBack = createButton("0. Terug", 5, 34);
    components.add(buttonBack);

    components.add(createLabel("Maak een keuze (0, 0, 0 of 0)", 1, 37));

    userInputArea = createTextArea(0, 38, 80, 2);
    components.add(userInputArea);

    return components;
  }
}
