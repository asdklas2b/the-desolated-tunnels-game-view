package nl.aimsites.nzbvuq.gameview.view;

import com.valkryst.VTerminal.Screen;
import com.valkryst.VTerminal.component.Button;
import com.valkryst.VTerminal.component.Component;
import com.valkryst.VTerminal.component.Label;
import lombok.Getter;
import nl.aimsites.nzbvuq.gameview.model.IObserver;
import nl.aimsites.nzbvuq.gameview.model.SearchLobbyModel;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyContract;

import java.util.ArrayList;
import java.util.List;

/**
 * View for SearchLobby screen.
 *
 * @author Max Neerken (MD.Neerken@student.han.nl)
 */
public class SearchLobbyView extends View implements IObserver<SearchLobbyModel> {

  private static final int LOBBIES_PER_PAGE = 10;
  private static final int LOWEST_PAGE = 0;

  @Getter
  private Button buttonBack;
  @Getter
  private Button buttonNextPage;
  @Getter
  private Button buttonPreviousPage;
  @Getter
  private List<Button> lobbyButtons;
  private List<Label> nPlayersLabels;
  private List<Label> lobbyIndexLabels;
  private int pageNumber;

  public SearchLobbyView(Screen screen) {
    super(screen);

    pageNumber = LOWEST_PAGE;
  }

  /**
   * Go to the previous page if possible.
   *
   * @param model The SearchLobbyModel that is queried for the lobbies.
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public void previousPage(SearchLobbyModel model) {
    if (pageNumber <= LOWEST_PAGE)
      return;

    pageNumber--;
    displayLobbies(model);
  }

  /**
   * Go to the next page if possible.
   *
   * @param model The SearchLobbyModel that is queried for the lobbies.
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public void nextPage(SearchLobbyModel model) {
    if (Math.ceil(model.getLobbyCount() / (float) LOBBIES_PER_PAGE) <= pageNumber + 1)
      return;

    pageNumber++;
    displayLobbies(model);
  }

  /**
   * Display the lobbies on the current page.
   *
   * @param model The SearchLobbyModel that is queried for the lobbies.
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public void displayLobbies(SearchLobbyModel model) {
    var lowerIndex = pageNumber * LOBBIES_PER_PAGE;
    var upperIndex = Math.min(pageNumber * LOBBIES_PER_PAGE + LOBBIES_PER_PAGE, model.getLobbyCount());

    displayLobbies(model.getLobbies(lowerIndex, upperIndex));
  }

  private void displayLobbies(List<LobbyContract> lobbies) {
    removeLobbyComponents();

    lobbyIndexLabels = new ArrayList<>();
    lobbyButtons = new ArrayList<>();
    nPlayersLabels = new ArrayList<>();

    for (var i = 0; i < lobbies.size(); i++) {
      lobbyIndexLabels.add(createLabel(String.valueOf(i + 1).concat("."), 5, 10 + i));
      lobbyButtons.add(createButton(lobbies.get(i).getName(), 9, 10 + i));
      nPlayersLabels.add(createLabel(String.valueOf(lobbies.get(i).getPlayersCount()), 56, 10 + i));
    }

    addLobbyComponents();
  }

  private void removeLobbyComponents() {
    if (lobbyButtons != null) {
      lobbyIndexLabels.forEach(this::removeComponent);
      lobbyButtons.forEach(this::removeComponent);
      nPlayersLabels.forEach(this::removeComponent);
    }
  }

  private void addLobbyComponents() {
    addComponents(lobbyIndexLabels);
    addComponents(lobbyButtons);
    addComponents(nPlayersLabels);
  }

  @Override
  protected List<Component> initializeComponents() {
    var components = new ArrayList<Component>();

    components.add(createLabel("Lobby zoeken", 5, 5));

    components.add(createLabel("# | Lobbynaam                  |                   Aantal spelers |", 5, 7));

    userInputArea = createTextArea(0, 38, 80, 2);
    components.add(userInputArea);

    buttonBack = createButton("0. Terug", 2, 36);
    components.add(buttonBack);

    components.add(createLabel("Kies een lobby   |", 2, 37));

    buttonNextPage = createButton("Volgende pagina (Page down)", 22, 37);
    components.add(buttonNextPage);

    components.add(createLabel("|", 51, 37));

    buttonPreviousPage = createButton("Vorige pagina (Page up)", 55, 37);
    components.add(buttonPreviousPage);

    return components;
  }

  /**
   * Returns the index of the first lobby on the current page.
   *
   * @return The index of the first lobby on the current page.
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public int getPageStart() {
    return pageNumber * LOBBIES_PER_PAGE;
  }

  @Override
  public void update(SearchLobbyModel changedObject) {
    displayLobbies(changedObject);
  }
}
