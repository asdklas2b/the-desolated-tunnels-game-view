package nl.aimsites.nzbvuq.gameview.view;

import com.valkryst.VTerminal.Screen;
import com.valkryst.VTerminal.component.Component;

import java.util.ArrayList;
import java.util.List;

public class AttributesView extends View {
  /**
   * Constructs a new View that fills the entire screen.
   *
   * @param screen The screen on which the view is displayed.
   */
  public AttributesView(Screen screen) {
    super(screen);
  }

  @Override
  protected List<Component> initializeComponents() {
    return new ArrayList<>();
  }
}
