package nl.aimsites.nzbvuq.gameview.view;

import com.valkryst.VTerminal.Screen;
import com.valkryst.VTerminal.component.Button;
import com.valkryst.VTerminal.component.Component;
import com.valkryst.VTerminal.component.Label;
import com.valkryst.VTerminal.component.TextArea;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tim Weening (T.Weening@student.han.nl)
 */
public class CreateLobbyView extends View {
  @Getter
  private Button buttonBack;

  @Getter
  private Button buttonCreateLobby;

  @Getter
  private TextArea userInputAreaLobbyName;

  @Getter
  private TextArea userInputAreaAmountPlayers;

  private Label errorLabel;

  /**
   * Constructs a new View that fills the entire screen.
   *
   * @param screen The screen on which the view is displayed.
   */
  public CreateLobbyView(Screen screen) {
    super(screen);
  }

  @Override
  protected List<Component> initializeComponents() {
    var components = new ArrayList<Component>();

    components.add(createLabel("Aanmaken Lobby", 5, 3));

    components.add(createLabel("Voer hieronder de naam van de lobby en het maximum aantal spelers in.", 5, 6));

    components.add(createLabel("Naam lobby:", 5, 18));
    userInputAreaLobbyName = createTextArea(30, 18, 40, 1);
    components.add(userInputAreaLobbyName);

    components.add(createLabel("Maximaal aantal spelers:", 5, 20));
    userInputAreaAmountPlayers = createTextArea(30, 20, 40, 1);
    components.add(userInputAreaAmountPlayers);

    buttonBack = createButton("0. Terug", 5, 34);
    components.add(buttonBack);

    buttonCreateLobby = createButton("1. Aanmaken lobby", 30, 34);
    components.add(buttonCreateLobby);

    components.add(createLabel("Maak een keuze: 1 of 0", 0, 37));

    userInputArea = createTextArea(0, 38, 80, 2);
    components.add(userInputArea);

    return components;
  }

  /**
   * Displays an error message on the screen when filling in incorrect values.
   *
   * @param errorMessage The error message that needs to be displayed.
   * @author Tim Weening (T.Weening@student.han.nl) & Ruben Eppink (RC.Eppink@student.han.nl)
   */
  public void displayErrorMessage(String errorMessage) {
    if (errorLabel != null) {
      SCREEN.removeComponent(errorLabel);
    }
    errorLabel = createLabel(errorMessage, 10, 22);
    SCREEN.addComponent(errorLabel);
  }
}
