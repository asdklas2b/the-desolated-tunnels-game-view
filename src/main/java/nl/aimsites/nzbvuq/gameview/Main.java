//package nl.aimsites.nzbvuq.gameview;
//
//import nl.aimsites.nzbvuq.chat.interfaces.ISendMessage;
//import nl.aimsites.nzbvuq.game.entities.items.Storage;
//import nl.aimsites.nzbvuq.game.inputhandler.parser.IInputHandler;
//import nl.aimsites.nzbvuq.game.inputhandler.parser.InputHandler;
//import nl.aimsites.nzbvuq.game.inputhandler.parser.IInputHandler;
//import nl.aimsites.nzbvuq.game.inputhandler.parser.InputHandler;
//import nl.aimsites.nzbvuq.gameview.contracts.required.chat.SendMessage;
//import nl.aimsites.nzbvuq.lobby.contracts.LobbyContract;
//import nl.aimsites.nzbvuq.lobby.contracts.LobbyListenerContract;
//import nl.aimsites.nzbvuq.lobby.contracts.LobbyManagerContract;
//import nl.aimsites.nzbvuq.lobby.contracts.LobbyUserContract;
//import nl.aimsites.nzbvuq.lobby.exceptions.LobbyException;

//
//import java.util.ArrayList;
//import java.util.List;
//
//public class Main {
//  public static final int MAX_PLAYERS = 5;
//  public static final int N_PLAYERS = 2;
//
//  /**
//   * Mocks required interfaces and starts the view.
//   * This main is only used for testing purposes.
//   *
//   * @param args Standard main parameter.
//   * @author Max Neerken (MD.Neerken@student.han.nl)
//   */
//  public static void main(String[] args) {
//    var gameView = new GameView(createLobbyManager(), createSendMessage(), createInputHandler());

//    gameView.start();
//  }
//
//  private static LobbyManagerContract createLobbyManager() {
//    return new LobbyManagerContract() {
//      @Override
//      public void listen(LobbyListenerContract listener) {
//      }
//
//      @Override
//      public List<LobbyContract> getLobbies() {
//        var lobbies = new ArrayList<LobbyContract>();
//        lobbies.add(createLobbyContract());
//        return lobbies;
//      }
//
//      @Override
//      public void join(LobbyContract lobby) {
//        System.out.println("player joined lobby");
//      }
//
//      @Override
//      public void leave() {
//        System.out.println("Player left lobby");
//      }
//
//      @Override
//      public void create(String name, int maxPlayers) {
//        System.out.println("Created lobby.");
//      }
//
//      @Override
//      public void disband() {
//        System.out.println("The lobby is disbanded.");
//      }
//
//      @Override
//      public void startGame() throws LobbyException {
//
//      }
//
//      @Override
//      public LobbyContract getOwnLobby() {
//        return createLobbyContract();
//      }
//
//      @Override
//      public LobbyContract getJoinedLobby() {
//        return createLobbyContract();
//      }
//
//      @Override
//      public void setUsername(String name) {
//        System.out.println("username set");
//      }
//    };
//  }
//

//  private static SendMessage createSendMessage() {
//    return new SendMessage() {
//      @Override
//      public void sendMessageToPeers(String chatMessage, ArrayList<String> peers, String sender) {
//      }
//    };

//  }
//
//  private static IInputHandler createInputHandler() {
//    return new InputHandler();
//  }
//
//  private static LobbyContract createLobbyContract() {
//    return new LobbyContract() {
//      @Override
//      public String getIdentifier() {
//        return "";
//      }
//
//      @Override
//      public String getName() {
//        return "lobby1";
//      }
//
//      @Override
//      public int getMaxPlayers() {
//        return MAX_PLAYERS;
//      }
//
//      @Override
//      public int getPlayersCount() {
//        return N_PLAYERS;
//      }
//
//      @Override
//      public List<LobbyUserContract> getPlayers() {
//        var players = new ArrayList<LobbyUserContract>();
//        players.add(new LobbyUserContract() {
//          @Override
//          public String getIdentifier() {
//            return "sdiofahgowieureho";
//          }
//
//          @Override
//          public String getName() {
//            return "Player1";
//          }
//        });
//        players.add(new LobbyUserContract() {
//          @Override
//          public String getIdentifier() {
//            return "sdiofahgowieureho";
//          }
//
//          @Override
//          public String getName() {
//            return "Player2";
//          }
//        });
//        return players;
//      }
//
//      @Override
//      public void addPlayer(LobbyUserContract player) {
//        System.out.println("player added");
//      }
//
//      @Override
//      public boolean isStarted() {
//        return false;
//      }
//
//      @Override
//      public void setStarted(boolean started) {
//        System.out.println("started game");
//      }
//    };
//  }
//}
