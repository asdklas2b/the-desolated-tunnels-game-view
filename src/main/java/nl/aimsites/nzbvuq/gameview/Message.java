package nl.aimsites.nzbvuq.gameview;

import com.valkryst.VTerminal.Tile;
import lombok.Getter;
import nl.aimsites.nzbvuq.game.inputhandler.PrintType;
import nl.aimsites.nzbvuq.chat.enums.ChatType;

import java.awt.*;

/**
 * @author Ruben Eppink (RC.Eppink@student.han.nl)
 *
 * This class is used to construct a message to be displayed on screen as either a chat message or game message.
 *
 * */
public class Message {
  /** The default background color of all messages. */
  public static Color DEFAULT_BACKGROUND_COLOR = new Color(0xFF8E999E);
  /** The default foreground color of all messages. */
  public static final Color DEFAULT_FOREGROUND_COLOR = new Color(0xFF68D0FF);
  @Getter ChatType chatType;
  @Getter private boolean isChatMessage;
  @Getter private String sender;
  /** The message. */
  @Getter private Tile[] message;
  @Getter private String textMessage;

  @Getter
  private PrintType output = PrintType.DEFAULT;

  /**
   * Constructs a new message.
   */
  public Message() {
    this.message = prepareString(0);
  }

  /**
   * Constructs a new message.
   *
   * @param message The message.
   * @param isChatMessage if it's chat message
   * @param sender of the message
   * @author Ruben Eppink (RC.Eppink@student.han.nl)
   */
  public Message(final String message, boolean isChatMessage, String sender, PrintType output, ChatType chatType) {
    this.isChatMessage = isChatMessage;
    this.sender = sender;
    this.output = output;
    if (message == null) {
      this.message = prepareString(0);
    } else {
      this.textMessage = sender + ": " + message;
      this.message = prepareString(sender + ": " + message);
    }
  }

  /**
   * Constructs a new message.
   *
   * @param message The message.
   * @param isChatMessage if it's chat message
   * @author Ruben Eppink (RC.Eppink@student.han.nl)
   */
  public Message(final String message, boolean isChatMessage, PrintType output) {
    this.output = output;
    if (message == null) {
      this.message = prepareString(0);
    } else {
      this.textMessage = message;
      this.message = prepareString(message);
    }
    this.isChatMessage = isChatMessage;
  }

  public Message(final String message, boolean isChatMessage) {
    this(message, isChatMessage, PrintType.DEFAULT);
  }

  /**
   * Prepares an array of tiles with the default message box colors.
   *
   * @param length The length of the array.
   * @return The array.
   * @author Ruben Eppink (RC.Eppink@student.han.nl)
   */
  public static Tile[] prepareString(final int length) {
    final Tile[] tiles = new Tile[length];

    for (int i = 0; i < tiles.length; i++) {
      tiles[i] = new Tile(' ');
      tiles[i].setBackgroundColor(DEFAULT_BACKGROUND_COLOR);
      tiles[i].setForegroundColor(DEFAULT_FOREGROUND_COLOR);
    }

    return tiles;
  }

  /**
   * Prepares a string with the default message box colors.
   *
   * @param text The text of the AsciiString,
   * @return The tiles.
   * @author Ruben Eppink (RC.Eppink@student.han.nl)
   */
  public static Tile[] prepareString(final String text) {
    if (text == null) {
      return new Tile[0];
    }

    final Tile[] tiles = new Tile[text.length()];

    for (int i = 0; i < text.length(); i++) {
      tiles[i] = new Tile(text.charAt(i));
      tiles[i].setCharacter(text.charAt(i));
      tiles[i].setBackgroundColor(DEFAULT_BACKGROUND_COLOR);
      tiles[i].setForegroundColor(DEFAULT_FOREGROUND_COLOR);
    }

    return tiles;
  }

  /**
   * Appends a string to the message.
   *
   * @param text The text.
   * @return This.
   * @author Ruben Eppink (RC.Eppink@student.han.nl)
   */
  public Message append(final String text) {
    if (text != null) {
      append(prepareString(text));
    }

    return this;
  }

  /**
   * Appends an array of tiles to the message.
   *
   * @param text The text.
   * @return This.
   * @author Ruben Eppink (RC.Eppink@student.han.nl)
   */
  public Message append(final Tile[] text) {
    if (text == null) {
      return this;
    }

    final Tile[] newMessage = new Tile[message.length + text.length];

    for (int i = 0; i < message.length; i++) {
      newMessage[i] = new Tile(' ');
      newMessage[i].copy(message[i]);
    }

    for (int i = message.length; i < newMessage.length; i++) {
      newMessage[i] = new Tile(' ');
      newMessage[i].copy(text[i - message.length]);
    }

    message = newMessage;

    return this;
  }
}
