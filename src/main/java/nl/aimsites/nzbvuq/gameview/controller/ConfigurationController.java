package nl.aimsites.nzbvuq.gameview.controller;

import com.valkryst.VTerminal.Screen;
import nl.aimsites.nzbvuq.gameview.model.ConfigurationModel;
import nl.aimsites.nzbvuq.gameview.model.Model;
import nl.aimsites.nzbvuq.gameview.view.ConfigurationView;
import nl.aimsites.nzbvuq.gameview.view.NameConfigurationView;
import nl.aimsites.nzbvuq.gameview.view.View;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Controller for Configuration.
 *
 * @author Tim Weening (T.Weening@student.han.nl)
 */
public class ConfigurationController extends Controller<ConfigurationView, ConfigurationModel> {

  /**
   * Constructs a new controller and its view and controller.
   *
   * @param screen             The window.
   * @param previousController The controller which created this controller.
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public ConfigurationController(Screen screen, Controller<? extends View, ? extends Model> previousController) {
    super(new ConfigurationView(screen), new ConfigurationModel(), screen, previousController);
  }


  @Override
  protected void initializeOnClickFunctions() {
    view.getButtonAgents().setOnClickFunction(() -> {
      exit();
      new AgentConfigurationController(screen, this);
    });

    view.getButtonMonsters().setOnClickFunction(() -> {
      exit();
      new MonsterConfigurationController(screen, this);
    });

    view.getButtonAttributes().setOnClickFunction(() -> {
      exit();
      new AttributeConfigurationController(screen, this);
    });

    view.getButtonName().setOnClickFunction(() -> {
      exit();
      new NameConfigurationController(screen, this);
    });

    view.getButtonBack().setOnClickFunction(this::back);
  }

  @Override
  protected KeyListener createKeyListener() {
    return new KeyListener() {

      @Override
      public void keyTyped(KeyEvent e) {
        //
      }

      @Override
      public void keyPressed(KeyEvent e) {
        //
      }

      @Override
      public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
          var playerInput = resetUserInputArea();
          switch (playerInput) {
            case "1" -> view.getButtonAgents().getOnClickFunction().run();
            case "2" -> view.getButtonMonsters().getOnClickFunction().run();
            case "3" -> view.getButtonAttributes().getOnClickFunction().run();
            case "0" -> view.getButtonBack().getOnClickFunction().run();
          }
        }
      }
    };
  }

  @Override
  public void enter() {
    //
  }

  @Override
  protected void exit() {
    super.removeFromScreen();
  }
}
