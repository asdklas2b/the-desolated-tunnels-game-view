package nl.aimsites.nzbvuq.gameview.controller;

import com.valkryst.VTerminal.Screen;
import nl.aimsites.nzbvuq.gameview.model.Model;
import nl.aimsites.nzbvuq.gameview.model.MonsterModel;
import nl.aimsites.nzbvuq.gameview.view.MonsterView;
import nl.aimsites.nzbvuq.gameview.view.View;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MonsterController extends Controller<MonsterView, MonsterModel> {

  /**
   * Constructs a new controller and its view and controller.
   *
   * @param screen             The window.
   * @param previousController The controller which created this controller.
   */
  public MonsterController(Screen screen, Controller<? extends View, ? extends Model> previousController) {
    super(new MonsterView(screen), new MonsterModel(), screen, previousController);
  }

  @Override
  protected void initializeOnClickFunctions() {
    //
  }

  @Override
  protected KeyListener createKeyListener() {
    return new KeyListener() {
      @Override
      public void keyTyped(KeyEvent e) {
        //
      }

      @Override
      public void keyPressed(KeyEvent e) {
        //
      }

      @Override
      public void keyReleased(KeyEvent e) {
        //
      }
    };
  }

  @Override
  public void enter() {
    //
  }

  @Override
  protected void exit() {
    super.removeFromScreen();
  }
}
