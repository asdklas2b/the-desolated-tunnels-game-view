package nl.aimsites.nzbvuq.gameview.controller;

import com.valkryst.VTerminal.Screen;
import nl.aimsites.nzbvuq.gameview.model.Model;
import nl.aimsites.nzbvuq.gameview.model.ResumeGameModel;
import nl.aimsites.nzbvuq.gameview.view.ResumeGameView;
import nl.aimsites.nzbvuq.gameview.view.View;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class ResumeGameController extends Controller<ResumeGameView, ResumeGameModel> {

  /**
   * Constructs a new controller and its view and controller.
   *
   * @param screen             The window.
   * @param previousController The controller which created this controller
   */
  public ResumeGameController(Screen screen, Controller<? extends View, ? extends Model> previousController) {
    super(new ResumeGameView(screen), new ResumeGameModel(), screen, previousController);
  }

  @Override
  protected void initializeOnClickFunctions() {
    //
  }

  @Override
  protected KeyListener createKeyListener() {
    return new KeyListener() {
      @Override
      public void keyTyped(KeyEvent e) {
        //
      }

      @Override
      public void keyPressed(KeyEvent e) {
        //
      }

      @Override
      public void keyReleased(KeyEvent e) {
        //
      }
    };
  }

  @Override
  public void enter() {
    //
  }

  @Override
  protected void exit() {
    super.removeFromScreen();
  }
}
