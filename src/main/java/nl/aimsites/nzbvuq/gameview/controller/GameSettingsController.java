package nl.aimsites.nzbvuq.gameview.controller;

import com.valkryst.VTerminal.Screen;
import nl.aimsites.nzbvuq.gameview.model.GameSettingsModel;
import nl.aimsites.nzbvuq.gameview.model.Model;
import nl.aimsites.nzbvuq.gameview.view.GameSettingsView;
import nl.aimsites.nzbvuq.gameview.view.View;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Controller for GameSettings
 * @author Ruben Eppink (RC.Eppink@student.han.nl)
 */
public class GameSettingsController extends Controller<GameSettingsView, GameSettingsModel> {

  /**
   * Constructs a new controller and its view and controller.
   *
   * @param screen             The window.
   * @param previousController The controller which created this controller.
   * @author Ruben Eppink (RC.Eppink@student.han.nl)
   */
  public GameSettingsController(Screen screen, Controller<? extends View, ? extends Model> previousController) {
    super(new GameSettingsView(screen), new GameSettingsModel(), screen, previousController);
  }

  @Override
  protected void initializeOnClickFunctions() {
    view.getButtonConfiguration().setOnClickFunction(() -> {
      exit();
      new ConfigurationController(screen, this);
    });

    view.getButtonAgents().setOnClickFunction(() -> {
      exit();
      new AgentsController(screen, this);
    });

    view.getButtonAttributes().setOnClickFunction(() -> {
      exit();
      new AttributesController(screen, this);
    });

    view.getButtonBack().setOnClickFunction(this::back);

    view.getButtonGameModes().setOnClickFunction(() -> {
      exit();
      new GamesModesController(screen, this);
    });

    view.getButtonMonsters().setOnClickFunction(() -> {
      exit();
      new MonsterController(screen, this);
    });

    view.getButtonResumeGame().setOnClickFunction(() -> {
      exit();
      new ResumeGameController(screen, this);
    });
  }

  @Override
  protected KeyListener createKeyListener() {
    return new KeyListener() {
      @Override
      public void keyTyped(KeyEvent e) {
        //
      }

      @Override
      public void keyPressed(KeyEvent e) {
        //
      }

      @Override
      public void keyReleased(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
          view.getButtonBack().getOnClickFunction().run();
        } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
          var playerInput = resetUserInputArea();
          switch (playerInput) {
            case "0" -> view.getButtonBack().getOnClickFunction().run();
            case "1" -> view.getButtonGameModes().getOnClickFunction().run();
            case "2" -> view.getButtonAgents().getOnClickFunction().run();
            case "3" -> view.getButtonMonsters().getOnClickFunction().run();
            case "4" -> view.getButtonAttributes().getOnClickFunction().run();
            case "5" -> view.getButtonConfiguration().getOnClickFunction().run();
            case "6" -> view.getButtonResumeGame().getOnClickFunction().run();
          }
        }
      }
    };
  }


  @Override
  public void enter() {
    //
  }

  @Override
  protected void exit() {
    super.removeFromScreen();
  }
}
