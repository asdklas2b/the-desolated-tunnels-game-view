package nl.aimsites.nzbvuq.gameview.controller;

import com.valkryst.VTerminal.Screen;
import nl.aimsites.nzbvuq.gameview.GameView;
import nl.aimsites.nzbvuq.gameview.model.Model;
import nl.aimsites.nzbvuq.gameview.model.NameModel;
import nl.aimsites.nzbvuq.gameview.view.NameConfigurationView;
import nl.aimsites.nzbvuq.gameview.view.View;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public class NameConfigurationController extends Controller<NameConfigurationView, NameModel> {

  public NameConfigurationController(Screen screen, Controller<? extends View, ? extends Model> previousController) {
    super(new NameConfigurationView(screen),
      new NameModel(),
      screen,
      previousController);
  }

  @Override
  protected void initializeOnClickFunctions() {
    view.getButtonBack().setOnClickFunction(this::back);

    view.getUserInputName()
      .appendText(GameView.getLobbyManager().getUsername());

    view.getButtonSave().setOnClickFunction(() -> {
      var playerName = view.getUserInputName().getText().trim();

      GameView.getLobbyManager().setUsername(playerName);

      exit();
      new ConfigurationController(screen, this.getPreviousController());
    });
  }

  @Override
  protected KeyListener createKeyListener() {
    return new KeyListener() {

      @Override
      public void keyTyped(KeyEvent e) {
        //
      }

      @Override
      public void keyPressed(KeyEvent e) {
        //
      }

      @Override
      public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
          var playerInput = resetUserInputArea();
          switch (playerInput) {
            case "0" -> view.getButtonBack().getOnClickFunction().run();
            case "1" -> view.getButtonSave().getOnClickFunction().run();
          }
        }
      }
    };
  }

  @Override
  public void enter() {
    //
  }

  @Override
  protected void exit() {
    super.removeFromScreen();
  }
}
