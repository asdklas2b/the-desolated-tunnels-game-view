package nl.aimsites.nzbvuq.gameview.controller;

import com.valkryst.VTerminal.Screen;
import nl.aimsites.nzbvuq.gameview.model.AgentConfigurationModel;
import nl.aimsites.nzbvuq.gameview.model.Model;
import nl.aimsites.nzbvuq.gameview.view.AgentConfigurationView;
import nl.aimsites.nzbvuq.gameview.view.View;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * @author Ruben Eppink (RC.Eppink@student.han.nl)
 *
 * This class is responsible for controlling the user input when the user is in
 * the agent configuration screen.
 *
 * */
public class AgentConfigurationController extends Controller<AgentConfigurationView, AgentConfigurationModel> {

  /**
   * Constructs a new controller and its view and controller.
   *
   * @param screen             The window.
   * @param previousController The controller which created this controller.
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public AgentConfigurationController(Screen screen, Controller<? extends View, ? extends Model> previousController) {
    super(new AgentConfigurationView(screen), new AgentConfigurationModel(), screen, previousController);
  }

  @Override
  protected void initializeOnClickFunctions() {
    view.getButtonBack().setOnClickFunction(this::back);
  }

  @Override
  protected KeyListener createKeyListener() {
    return new KeyListener() {
      @Override
      public void keyTyped(KeyEvent e) {
        //
      }

      @Override
      public void keyPressed(KeyEvent e) {
        //
      }

      @Override
      public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
          var playerInput = resetUserInputArea();
          switch (playerInput) {
            case "0" -> view.getButtonBack().getOnClickFunction().run();
            default -> model.convert(playerInput);
          }
        }
      }
    };
  }

  @Override
  public void enter() {
    //
  }

  @Override
  protected void exit() {
    super.removeFromScreen();
  }
}
