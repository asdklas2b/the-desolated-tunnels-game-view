package nl.aimsites.nzbvuq.gameview.controller;

import com.valkryst.VTerminal.Screen;
import nl.aimsites.nzbvuq.gameview.GameView;
import nl.aimsites.nzbvuq.gameview.model.IObserver;
import nl.aimsites.nzbvuq.gameview.model.Model;
import nl.aimsites.nzbvuq.gameview.model.SearchLobbyModel;
import nl.aimsites.nzbvuq.gameview.view.SearchLobbyView;
import nl.aimsites.nzbvuq.gameview.view.View;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Controller for the SearchLobby screen.
 *
 * @author Max Neerken (MD.Neerken@student.han.nl)
 */
public class SearchLobbyController extends Controller<SearchLobbyView, SearchLobbyModel> implements IObserver<SearchLobbyModel> {

  /**
   * Constructs a new controller and its view and controller.
   *
   * @param screen             The window.
   * @param previousController The controller which created this controller.
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public SearchLobbyController(Screen screen, Controller<? extends View, ? extends Model> previousController) {
    super(new SearchLobbyView(screen), new SearchLobbyModel(GameView.getLobbyManager()), screen, previousController);
    model.setController(this);
  }

  /**
   * Initialises actions that are run when a lobby button is pressed.
   * When a lobby button is pressed the lobby screen is shown.
   *
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  private void initializeLobbyButtons() {
    view.getLobbyButtons().forEach(button -> button.setOnClickFunction(() -> {
      exit();

      var index = view.getPageStart() + view.getLobbyButtons().indexOf(button);
      model.joinLobby(model.getLobby(index));
    }));
  }

  @Override
  protected void initializeOnClickFunctions() {
    view.getButtonBack().setOnClickFunction(this::back);

    view.getButtonPreviousPage().setOnClickFunction(() -> {
      view.previousPage(model);
      initializeLobbyButtons();
    });

    view.getButtonNextPage().setOnClickFunction(() -> {
      view.nextPage(model);
      initializeLobbyButtons();
    });
  }

  @Override
  protected KeyListener createKeyListener() {
    return new KeyListener() {

      @Override
      public void keyTyped(KeyEvent e) {
        //
      }

      @Override
      public void keyPressed(KeyEvent e) {
        //
      }

      @Override
      public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
          case KeyEvent.VK_ESCAPE -> view.getButtonBack().getOnClickFunction().run();
          case KeyEvent.VK_PAGE_UP -> view.getButtonPreviousPage().getOnClickFunction().run();
          case KeyEvent.VK_PAGE_DOWN -> view.getButtonNextPage().getOnClickFunction().run();
          case KeyEvent.VK_ENTER -> {
            var playerInput = resetUserInputArea();
            switch (playerInput) {
              case "0" -> view.getButtonBack().getOnClickFunction().run();
              case "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" -> view.getLobbyButtons().get(Integer.parseInt(playerInput) - 1).getOnClickFunction().run();
            }
          }
        }
      }
    };
  }

  @Override
  public void enter() {
    model.addObserver(view);
    model.addObserver(this);

    view.displayLobbies(model);
    initializeLobbyButtons();
  }

  @Override
  protected void exit() {
    model.removeObserver(view);
    model.removeObserver(this);

    super.removeFromScreen();
  }

  @Override
  public void update(SearchLobbyModel changedObject) {
    initializeLobbyButtons();
  }

  /**
   * Call this function to go to the lobby screen of the game that is joined.
   *
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public void joinGame() {
    new LobbyController(screen, this);
  }
}
