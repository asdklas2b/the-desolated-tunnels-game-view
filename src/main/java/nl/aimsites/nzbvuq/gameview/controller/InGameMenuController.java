package nl.aimsites.nzbvuq.gameview.controller;

import com.valkryst.VTerminal.Screen;
import nl.aimsites.nzbvuq.gameview.GameView;
import nl.aimsites.nzbvuq.gameview.model.InGameMenuModel;
import nl.aimsites.nzbvuq.gameview.model.Model;
import nl.aimsites.nzbvuq.gameview.view.InGameMenuView;
import nl.aimsites.nzbvuq.gameview.view.View;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Controller for InGameMenu.
 * @author Tim Weening (T.Weening@student.han.nl)
 */
public class InGameMenuController extends Controller<InGameMenuView, InGameMenuModel> {

  /**
   * Constructs a new controller and its view and controller.
   *
   * @param screen             The window.
   * @param previousController The controller which created this controller.
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public InGameMenuController(Screen screen, Controller previousController) {
    super(new InGameMenuView(screen), new InGameMenuModel(GameView.getUseAgent()), screen, previousController);
  }

  @Override
  protected void initializeOnClickFunctions() {
    view.getButtonSwapAgent().setOnClickFunction(model::swapAgent);

    view.getButtonConfiguration().setOnClickFunction(() -> {
      exit();
      new ConfigurationController(screen, this);
    });

    view.getButtonSaveAndExit().setOnClickFunction(() -> {
      exit();
      new MainMenuController(screen);
    });

    view.getButtonResumeGame().setOnClickFunction(this::back);
  }

  @Override
  protected KeyListener createKeyListener() {
    return new KeyListener() {

      @Override
      public void keyTyped(KeyEvent e) {
        //
      }

      @Override
      public void keyPressed(KeyEvent e) {
        //
      }

      @Override
      public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
          case KeyEvent.VK_ESCAPE -> view.getButtonResumeGame().getOnClickFunction().run();
          case KeyEvent.VK_ENTER -> {
            var playerInput = resetUserInputArea();
            switch (playerInput) {
              case "0" -> view.getButtonResumeGame().getOnClickFunction().run();
              case "1" -> view.getButtonSwapAgent().getOnClickFunction().run();
              case "2" -> view.getButtonConfiguration().getOnClickFunction().run();
              case "3" -> view.getButtonSaveAndExit().getOnClickFunction().run();
            }
          }
        }
      }
    };
  }

  @Override
  public void enter() {
    //
  }

  @Override
  protected void exit() {
    super.removeFromScreen();
  }
}
