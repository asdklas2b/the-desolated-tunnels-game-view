package nl.aimsites.nzbvuq.gameview.controller;

import com.valkryst.VTerminal.Screen;
import lombok.Getter;
import lombok.NonNull;
import nl.aimsites.nzbvuq.gameview.model.Model;
import nl.aimsites.nzbvuq.gameview.view.View;

import java.awt.event.KeyListener;
import java.util.EventListener;

/**
 * This abstract class can be extended by controller classes that process input.
 *
 * @param <V> The type of View that corresponds to the controller.
 * @param <M> The type of Model that corresponds to the controller.
 * @author Max Neerken MD.Neerken@student.han.nl
 * @author Ruben Eppink (RC.Eppink@student.han.nl)
 */
public abstract class Controller<V extends View, M extends Model> {

  protected final V view;
  protected final M model;
  protected final Screen screen;
  @Getter
  private final Controller<? extends View, ? extends Model> previousController;
  protected EventListener keyListener;

  /**
   * Constructs a new Controller.
   *
   * @param view               The view.
   * @param model              The model.
   * @param screen             The window.
   * @param previousController The controller which created this controller.
   * @throws NullPointerException If the view, model or screen is null.
   * @author Ruben Eppink (RC.Eppink@student.han.nl) & Max Neerken (MD.Neerken@student.han.nl)
   */
  protected Controller(final @NonNull V view, final @NonNull M model, @NonNull Screen screen, Controller<? extends View, ? extends Model> previousController) {
    this.view = view;
    this.model = model;
    this.screen = screen;

    this.previousController = previousController;

    initializeEventHandlers();
    this.screen.draw();
    addToScreen();

    enter();
  }

  /**
   * Adds the view to a screen after removing all other components from the screen.
   *
   * @author Ruben Eppink (RC.Eppink@student.han.nl) & Max Neerken (MD.Neerken@student.han.nl)
   */
  public void addToScreen() {
    if (screen == null) {
      return;
    }

    screen.addComponent(view);

    screen.addListener(keyListener);
  }

  /**
   * Removes the view from a screen.
   *
   * @author Ruben Eppink (RC.Eppink@student.han.nl) & Max Neerken (MD.Neerken@student.han.nl)
   */
  public void removeFromScreen() {
    if (screen == null)
      return;

    screen.removeComponent(view);

    screen.removeListener(keyListener);
  }

  /**
   * Creates actions that are run on given events.
   *
   * @author Ruben Eppink (RC.Eppink@student.han.nl) & Max Neerken (MD.Neerken@student.han.nl)
   */
  private void initializeEventHandlers() {
    initializeOnClickFunctions();
    keyListener = createKeyListener();
  }

  /**
   * Use this method to declare what actions
   * buttons should do when pressed.
   *
   * @author Max Neerken (MD.Neerken@student.han.nl
   */
  protected abstract void initializeOnClickFunctions();

  /**
   * Creates a KeyListener that performs actions
   * when certain keys are pressed.
   *
   * @return The KeyListener.
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  protected abstract KeyListener createKeyListener();

  /**
   * Empties the user input text area, returns user input without leading and trailing
   * white spaces and returns caret to the start.
   *
   * @return The input from the text area.
   * @author Ruben Eppink & Max Neerken (MD.Neerken@student.han.nl)
   */
  protected String resetUserInputArea() {
    var playerInput = view.getUserInputArea().getText().trim();
    view.getUserInputArea().clearText();
    view.getUserInputArea().changeCaretPosition(0, 0);
    return playerInput;
  }

  /**
   * Return to the previous view or if no
   * previous view is set return to the main menu.
   *
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  protected void back() {
    if (previousController == null) {
      exit();
      var mainMenuController = new MainMenuController(screen);
      removeFromScreen();
      mainMenuController.addToScreen();
      mainMenuController.enter();
      screen.draw();
    } else {
      exit();
      removeFromScreen();
      previousController.addToScreen();
      previousController.enter();
      screen.draw();
    }
  }

  /**
   * Initialisation method to be called when entering the screen.
   *
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public abstract void enter();

  /**
   * Cleanup method to be called when exiting the screen.
   *
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  protected abstract void exit();
}
