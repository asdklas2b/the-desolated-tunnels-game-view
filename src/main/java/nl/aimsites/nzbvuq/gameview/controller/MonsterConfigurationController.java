package nl.aimsites.nzbvuq.gameview.controller;

import com.valkryst.VTerminal.Screen;
import nl.aimsites.nzbvuq.gameview.model.Model;
import nl.aimsites.nzbvuq.gameview.model.MonsterConfigurationModel;
import nl.aimsites.nzbvuq.gameview.view.MonsterConfigurationView;
import nl.aimsites.nzbvuq.gameview.view.View;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MonsterConfigurationController extends Controller<MonsterConfigurationView, MonsterConfigurationModel> {

  /**
   * Constructs a new controller and its view and controller.
   *
   * @param screen             The window.
   * @param previousController The controller which created this controller.
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public MonsterConfigurationController(Screen screen, Controller<? extends View, ? extends Model> previousController) {
    super(new MonsterConfigurationView(screen), new MonsterConfigurationModel(), screen, previousController);
  }

  @Override
  protected void initializeOnClickFunctions() {
    view.getButtonBack().setOnClickFunction(this::back);
  }

  @Override
  protected KeyListener createKeyListener() {
    return new KeyListener() {
      @Override
      public void keyTyped(KeyEvent e) {
        //
      }

      @Override
      public void keyPressed(KeyEvent e) {
        //
      }

      @Override
      public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
          var playerInput = resetUserInputArea();
          switch (playerInput) {
            case "0" -> view.getButtonBack().getOnClickFunction().run();
          }
        }
      }
    };
  }

  @Override
  public void enter() {
    //
  }

  @Override
  protected void exit() {
    super.removeFromScreen();
  }
}
