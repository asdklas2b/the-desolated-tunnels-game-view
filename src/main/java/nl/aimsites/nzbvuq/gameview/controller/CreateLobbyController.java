package nl.aimsites.nzbvuq.gameview.controller;

import com.valkryst.VTerminal.Screen;
import nl.aimsites.nzbvuq.gameview.model.CreateLobbyModel;
import nl.aimsites.nzbvuq.gameview.model.Model;
import nl.aimsites.nzbvuq.gameview.view.CreateLobbyView;
import nl.aimsites.nzbvuq.gameview.view.View;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Controller for CreateLobby.
 *
 * @author Tim Weening (T.Weening@student.han.nl)
 */
public class CreateLobbyController extends Controller<CreateLobbyView, CreateLobbyModel> {

  /**
   * Constructs a new controller and its view and controller.
   *
   * @param screen             The window.
   * @param previousController The controller which created this controller.
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public CreateLobbyController(Screen screen, Controller<? extends View, ? extends Model> previousController) {
    super(new CreateLobbyView(screen), new CreateLobbyModel(), screen, previousController);
  }

  @Override
  protected void initializeOnClickFunctions() {
    view.getButtonBack().setOnClickFunction(this::back);

    view.getButtonCreateLobby().setOnClickFunction(() -> {
      String lobbyName = view.getUserInputAreaLobbyName().getText().trim();
      String maxAmountOfPlayers = view.getUserInputAreaAmountPlayers().getText().trim();

      if (validateInput(lobbyName, maxAmountOfPlayers)) {
        model.createLobby(lobbyName, Integer.parseInt(maxAmountOfPlayers));
        exit();
        new LobbyController(screen, this.getPreviousController());
      }

    });
  }

  @Override
  protected KeyListener createKeyListener() {
    return new KeyListener() {

      @Override
      public void keyTyped(KeyEvent e) {
        //
      }

      @Override
      public void keyPressed(KeyEvent e) {
        //
      }

      @Override
      public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
          var playerInput = resetUserInputArea();
          switch (playerInput) {
            case "0" -> view.getButtonBack().getOnClickFunction().run();
            case "1" -> view.getButtonCreateLobby().getOnClickFunction().run();
          }
        }
      }
    };
  }

  /**
   * Validates the input of the Lobby Name and the maximum amount of players.
   *
   * @param lobbyName          The name of the lobby.
   * @param maxAmountOfPlayers The maximum amount of players.
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  private boolean validateInput(String lobbyName, String maxAmountOfPlayers) {
    //TODO Errormeldingen rood maken en mogelijk centreren.
    if (lobbyName.isEmpty() || maxAmountOfPlayers.isEmpty()) {
      view.displayErrorMessage("De naam van de lobby en/of het aantal spelers is niet ingevuld.");
      return false;
    }

    try {
      if (!model.checkMaxAmountOfPlayersValid(Integer.parseInt(maxAmountOfPlayers))) {
        view.displayErrorMessage("Het aantal spelers moet tussen de 1 en de 20 liggen.");
      } else {
        return true;
      }
    } catch (NumberFormatException e) {
      view.displayErrorMessage("Het aantal spelers moet een getal zijn.");
    }
    return false;
  }

  @Override
  public void enter() {
    //
  }

  @Override
  protected void exit() {
    super.removeFromScreen();
  }
}
