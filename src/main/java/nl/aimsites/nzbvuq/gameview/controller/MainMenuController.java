package nl.aimsites.nzbvuq.gameview.controller;

import com.valkryst.VTerminal.Screen;
import nl.aimsites.nzbvuq.gameview.model.MainMenuModel;
import nl.aimsites.nzbvuq.gameview.view.MainMenuView;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Controller for MainMenu.
 *
 * @author Ruben Eppink (RC.Eppink@student.han.nl) & Max Neerken (MD.Neerken@student.han.nl)
 */
public class MainMenuController extends Controller<MainMenuView, MainMenuModel> {

  /**
   * Constructs a new controller and its view and controller.
   *
   * @param screen The window.
   * @author Ruben Eppink (RC.Eppink@student.han.nl) & Max Neerken (MD.Neerken@student.han.nl)
   */
  public MainMenuController(Screen screen) {
    super(new MainMenuView(screen), new MainMenuModel(), screen, null);
  }

  @Override
  protected void initializeOnClickFunctions() {
    view.getButtonCreateLobby().setOnClickFunction(() -> {
      exit();
      new CreateLobbyController(screen, this);
    });

    view.getButtonSearchLobby().setOnClickFunction(() -> {
      exit();
      new SearchLobbyController(screen, this);
    });

    view.getButtonConfiguration().setOnClickFunction(() -> {
      exit();
      new ConfigurationController(screen, this);
    });

    view.getButtonExit().setOnClickFunction(() -> System.exit(0));
  }

  @Override
  protected KeyListener createKeyListener() {
    return new KeyListener() {

      @Override
      public void keyTyped(KeyEvent e) {
        //
      }

      @Override
      public void keyPressed(KeyEvent e) {
        //
      }

      @Override
      public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
          var playerInput = resetUserInputArea();
          switch (playerInput) {
            case "1" -> view.getButtonCreateLobby().getOnClickFunction().run();
            case "2" -> view.getButtonSearchLobby().getOnClickFunction().run();
            case "3" -> view.getButtonConfiguration().getOnClickFunction().run();
            case "0" -> view.getButtonExit().getOnClickFunction().run();
          }
        }
      }
    };
  }

  @Override
  public void enter() {
    //
  }

  @Override
  protected void exit() {
    super.removeFromScreen();
  }
}
