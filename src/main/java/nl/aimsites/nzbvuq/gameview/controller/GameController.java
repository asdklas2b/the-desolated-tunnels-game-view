package nl.aimsites.nzbvuq.gameview.controller;

import com.valkryst.VTerminal.Screen;
import nl.aimsites.nzbvuq.game.inputhandler.PrintType;
import nl.aimsites.nzbvuq.game.state.GameState;
import nl.aimsites.nzbvuq.gameview.Message;
import nl.aimsites.nzbvuq.gameview.model.GameModel;
import nl.aimsites.nzbvuq.gameview.model.Model;
import nl.aimsites.nzbvuq.gameview.view.GameView;
import nl.aimsites.nzbvuq.gameview.view.View;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * Controller for Game.
 *
 * @author Ruben Eppink (RC.Eppink@student.han.nl)
 */
public class GameController extends Controller<GameView, GameModel> {
  private static GameController instance;

  /**
   * Constructs a new controller and its view and controller.
   *
   * @author Ruben Eppink (RC.Eppink@student.han.nl)
   * @param screen The window.
   * @throws NullPointerException If the view or model are null.
   */
  private GameController(Screen screen, Controller<? extends View, ? extends Model> previousController){
    super(new GameView(screen), GameModel.getInstance(), screen, previousController);
  }

  /**
   * Singleton pattern getinstance of gamecontroller
   *
   * @param screen             The window.
   * @param previousController The controller which created this controller.
   * @author Ruben Eppink (RC.Eppink@student.han.nl)
   * @author Dave Quentin (Dm.Quentin@student.han.nl)
   */
  public static GameController getInstance(Screen screen, Controller previousController){
    if (instance == null) {
      instance = new GameController(screen, previousController);
    }
    instance.model.addObserver(instance.view);
    return instance;
  }

  @Override
  protected void initializeOnClickFunctions() {
    view.getButtonPause().setOnClickFunction(() -> {
      exit();
      new InGameMenuController(screen, this);
    });
  }

  @Override
  protected KeyListener createKeyListener() {
    return new KeyListener() {
      @Override
      public void keyTyped(KeyEvent e) {
        //
      }

      @Override
      public void keyPressed(KeyEvent e) {
        //
      }

      @Override
      public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
          case KeyEvent.VK_ESCAPE -> view.getButtonPause().getOnClickFunction().run();
          case KeyEvent.VK_ENTER -> {
            if (GameState.getInstance().getPrinter() == null) {
              GameState.getInstance().setPrinter(view);
            }

            var playerInput = resetUserInputArea();
            if(playerInput.startsWith("/g")) {
              nl.aimsites.nzbvuq.gameview.GameView.getSendMessage().sendMessageToGlobal(nl.aimsites.nzbvuq.gameview.GameView.getLobbyManager().getUsername(), playerInput.substring(2));
            } else if (playerInput.startsWith("/t")) {
              nl.aimsites.nzbvuq.gameview.GameView.getSendMessage().sendMessageToTeam(nl.aimsites.nzbvuq.gameview.GameView.getLobbyManager().getUsername(), playerInput.substring(2));
              //TODO put text in chat area with "[team]" infront. example: "Ruben [team]: capture the flag!"
            } else {
              var scanner = new Scanner(new ByteArrayInputStream(playerInput.getBytes(StandardCharsets.UTF_8)));

              nl.aimsites.nzbvuq.gameview.GameView.getInputHandler()
                .handleInput(
                  scanner
                );
            }
          }
        }
      }
    };
  }

  @Override
  public void enter() {
    //
  }

  @Override
  protected void exit() {
    super.removeFromScreen();
  }

//  @Override
//  public void receiveInput(String input) {
//    var scanner = new Scanner(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)));
//
//    view.update(
//      new Message(
//        nl.aimsites.nzbvuq.gameview.GameView.getInputHandler()
//          .handleInput(
//            scanner
//          ),
//        false,
//        PrintType.INPUT
//      )
//    );
//  }
}
