package nl.aimsites.nzbvuq.gameview.controller;

import com.valkryst.VTerminal.Screen;
import nl.aimsites.nzbvuq.gameview.GameView;
import nl.aimsites.nzbvuq.gameview.model.LobbyModel;
import nl.aimsites.nzbvuq.gameview.model.Model;
import nl.aimsites.nzbvuq.gameview.view.LobbyView;
import nl.aimsites.nzbvuq.gameview.view.View;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Controller for the Lobby screen.
 *
 * @author Max Neerken (MD.Neerken@student.han.nl)
 */
public class LobbyController extends Controller<LobbyView, LobbyModel> {
  /**
   * Creates a new LobbyController.
   *
   * @param screen             The window.
   * @param previousController The controller which created this controller.
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public LobbyController(Screen screen, Controller<? extends View, ? extends Model> previousController) {
    super(new LobbyView(screen), new LobbyModel(GameView.getLobbyManager()), screen, previousController);
    model.setController(this);
  }

  @Override
  protected void initializeOnClickFunctions() {
    view.getButtonLeaveLobby().setOnClickFunction(() -> {
      model.leaveLobby();
      back();
    });

    if (model.isLobbyOwner()) {
      initializeLobbyOwnerOnClickFunctions();
    }
  }

  @Override
  protected KeyListener createKeyListener() {
    return new KeyListener() {

      @Override
      public void keyTyped(KeyEvent e) {
        //
      }

      @Override
      public void keyPressed(KeyEvent e) {
        //
      }

      @Override
      public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
          var playerInput = resetUserInputArea();
          switch (playerInput) {
            case "0" -> view.getButtonLeaveLobby().getOnClickFunction().run();
          }

          if (model.isLobbyOwner()) {
            switch (playerInput) {
              case "1" -> view.getButtonStartGame().getOnClickFunction().run();
              case "2" -> view.getButtonGameSettings().getOnClickFunction().run();
            }
          }
        }
      }
    };
  }

  /**
   * Initializes button functions which are only available
   * to the lobby owner.
   *
   * @author Max Neerken & Ruben Eppink (MD.Neerken@student.han.nl) & (RC.Eppink@student.han.nl)
   */
  private void initializeLobbyOwnerOnClickFunctions() {
    view.showLobbyOwnerComponents();

    view.getButtonStartGame().setOnClickFunction(model::startGame);

    view.getButtonGameSettings().setOnClickFunction(() -> {
      exit();
      new GameSettingsController(screen, this);
    });

    screen.draw();
    addToScreen();
  }

  /**
   * Sends the player to the GameView.
   *
   * @author Max Neerken (MD.Neerken@student.han.nl)
   */
  public void startGame() {
    exit();
    GameController.getInstance(screen, this);
  }

  @Override
  public void enter() {
    model.addObserver(view);
    view.displayLobbyData(model);
  }

  @Override
  protected void exit() {
    model.removeObserver(view);
    super.removeFromScreen();
  }
}
