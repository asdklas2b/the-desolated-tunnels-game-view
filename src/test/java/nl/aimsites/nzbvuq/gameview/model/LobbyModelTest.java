package nl.aimsites.nzbvuq.gameview.model;

import nl.aimsites.nzbvuq.gameview.controller.LobbyController;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyManagerContract;
import nl.aimsites.nzbvuq.lobby.exceptions.LobbyException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class LobbyModelTest {

  private static final String JOINED_LOBBY_ID = "JOINED_LOBBY_ID";
  private static final String OTHER_LOBBY_ID = "OTHER_LOBBY_ID";
  private LobbyModel sut;
  private LobbyManagerContract mockedLobbyManager;
  private LobbyController mockedController;
  private LobbyContract mockedJoinedLobby;

  @BeforeEach
  void setUp() {
    mockedJoinedLobby = Mockito.mock(LobbyContract.class);
    Mockito.when(mockedJoinedLobby.getIdentifier()).thenReturn(JOINED_LOBBY_ID);

    mockedLobbyManager = Mockito.mock(LobbyManagerContract.class);
    Mockito.when(mockedLobbyManager.getJoinedLobby()).thenReturn(mockedJoinedLobby);

    sut = new LobbyModel(mockedLobbyManager);

    mockedController = Mockito.mock(LobbyController.class);
    sut.setController(mockedController);
  }

  @Test
  void testIsLobbyOwnerCallsLobbyManagerContractGetOwnLobby() {
    // Arrange

    // Act
    sut.isLobbyOwner();

    // Assert
    Mockito.verify(mockedLobbyManager).getOwnLobby();
  }

  @Test
  void testIsLobbyOwnerReturnsTrueIfLobbyManagerContractGetOwnLobbyReturnsLobby() {
    // Arrange
    var returnedLobby = Mockito.mock(LobbyContract.class);
    Mockito.when(mockedLobbyManager.getOwnLobby()).thenReturn(returnedLobby);

    // Act
    var actual = sut.isLobbyOwner();

    // Assert
    Assertions.assertTrue(actual);
  }

  @Test
  void testIsLobbyOwnerReturnsFalseIfLobbyManagerContractGetOwnLobbyReturnsNull() {
    // Arrange
    Mockito.when(mockedLobbyManager.getOwnLobby()).thenReturn(null);

    // Act
    var actual = sut.isLobbyOwner();

    // Assert
    Assertions.assertFalse(actual);
  }

  @Test
  void testStartGameCallsLobbyManagerStartGame() throws LobbyException {
    // Arrange

    // Act
    sut.startGame();

    // Assert
    Mockito.verify(mockedLobbyManager).startGame();
  }

  @Test
  void testStartGameThrowsLobbyNotFoundExceptionIfLobbyExceptionIsReceived() throws LobbyException {
    // Arrange
    Mockito.doThrow(LobbyException.class).when(mockedLobbyManager).startGame();

    // Act & assert
    Assertions.assertThrows(LobbyNotFoundException.class, sut::startGame);
  }

  @Test
  void testGetLobbyCallsLobbyManagerGetJoinedLobby() {
    // Arrange
    Mockito.when(mockedLobbyManager.getJoinedLobby()).thenReturn(mockedJoinedLobby);

    // Act
    sut.getLobby();

    // Assert
    Mockito.verify(mockedLobbyManager, Mockito.times(2)).getJoinedLobby();
  }

  @Test
  void testGetLobbyReturnsLobbyManagerGetLobby() {
    // Arrange
    Mockito.when(mockedLobbyManager.getJoinedLobby()).thenReturn(mockedJoinedLobby);
    var expected = mockedJoinedLobby;

    // Act
    var actual = sut.getLobby();

    // Assert
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void testGetLobbyThrowsLobbyNotFoundExceptionIfJoinedLobbyIsNull() {
    // Arrange
    Mockito.when(mockedLobbyManager.getJoinedLobby()).thenReturn(null);

    // Act & assert
    Assertions.assertThrows(LobbyNotFoundException.class, sut::getLobby);
  }

  @Test
  void testLeaveLobbyCallsLobbyManagerDisbandLobbyIfLobbyManagerGetOwnLobbyIsNotNull() {
    // Arrange
    Mockito.when(mockedLobbyManager.getOwnLobby()).thenReturn(mockedJoinedLobby);

    // Act
    sut.leaveLobby();

    // Assert
    Mockito.verify(mockedLobbyManager).disband();
  }

  @Test
  void testLeaveLobbyCallsLobbyManagerLeaveLobbyIfLobbyManagerGetOwnLobbyIsNull() {
    // Arrange
    Mockito.when(mockedLobbyManager.getOwnLobby()).thenReturn(null);

    // Act
    sut.leaveLobby();

    // Assert
    Mockito.verify(mockedLobbyManager).leave();
  }

  @Test
  void testAddObserverAddsObserver() {
    // Arrange
    var expected = Mockito.mock(IObserver.class);

    // Act
    sut.addObserver(expected);
    var actual = sut.observers.get(0);

    // Assert
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void testRemoveObserverRemovesObserver() {
    // Arrange
    var mockedObserver = Mockito.mock(IObserver.class);
    var expected = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver);
    sut.observers.add(expected);

    // Act
    sut.removeObserver(mockedObserver);
    var actual = sut.observers.get(0);

    // Assert
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void testRemoveObserverWhenNoObserverPresent() {
    // Arrange
    var mockedObserver = Mockito.mock(IObserver.class);

    // Act & assert
    Assertions.assertDoesNotThrow(() -> sut.removeObserver(mockedObserver));
  }

  @Test
  void testNotifyObserversNotifiesObservers() {
    // Arrange
    var mockedObserver1 = Mockito.mock(IObserver.class);
    var mockedObserver2 = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver1);
    sut.observers.add(mockedObserver2);

    // Act
    sut.notifyObservers(sut);

    // Assert
    Mockito.verify(mockedObserver1).update(sut);
    Mockito.verify(mockedObserver2).update(sut);
  }

  @Test
  void testOnAddNotifiesObserversIfLobbyIsThisLobby() {
    // Arrange
    var mockedObserver1 = Mockito.mock(IObserver.class);
    var mockedObserver2 = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver1);
    sut.observers.add(mockedObserver2);

    // Act
    sut.onAdd(mockedJoinedLobby);

    // Assert
    Mockito.verify(mockedObserver1).update(sut);
    Mockito.verify(mockedObserver2).update(sut);
  }

  @Test
  void testOnAddDoesNotNotifyObserversIfLobbyIsNotThisLobby() {
    // Arrange
    var mockedObserver1 = Mockito.mock(IObserver.class);
    var mockedObserver2 = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver1);
    sut.observers.add(mockedObserver2);

    var mockedLobby = Mockito.mock(LobbyContract.class);
    Mockito.when(mockedLobby.getIdentifier()).thenReturn(OTHER_LOBBY_ID);

    // Act
    sut.onAdd(mockedLobby);

    // Assert
    Mockito.verify(mockedObserver1, Mockito.never()).update(sut);
    Mockito.verify(mockedObserver2, Mockito.never()).update(sut);
  }

  @Test
  void testOnUpdateNotifiesObserversIfLobbyIsThisLobby() {
    // Arrange
    var mockedObserver1 = Mockito.mock(IObserver.class);
    var mockedObserver2 = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver1);
    sut.observers.add(mockedObserver2);

    // Act
    sut.onUpdate(mockedJoinedLobby);

    // Assert
    Mockito.verify(mockedObserver1).update(sut);
    Mockito.verify(mockedObserver2).update(sut);
  }

  @Test
  void testOnUpdateDoesNotNotifyObserversIfLobbyIsNotThisLobby() {
    // Arrange
    var mockedObserver1 = Mockito.mock(IObserver.class);
    var mockedObserver2 = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver1);
    sut.observers.add(mockedObserver2);

    var mockedLobby = Mockito.mock(LobbyContract.class);
    Mockito.when(mockedLobby.getIdentifier()).thenReturn(OTHER_LOBBY_ID);

    // Act
    sut.onUpdate(mockedLobby);

    // Assert
    Mockito.verify(mockedObserver1, Mockito.never()).update(sut);
    Mockito.verify(mockedObserver2, Mockito.never()).update(sut);
  }

  @Test
  void testOnRemoveNotifiesObserversIfLobbyIsThisLobby() {
    // Arrange
    var mockedObserver1 = Mockito.mock(IObserver.class);
    var mockedObserver2 = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver1);
    sut.observers.add(mockedObserver2);

    // Act
    sut.onRemove(mockedJoinedLobby);

    // Assert
    Mockito.verify(mockedObserver1).update(sut);
    Mockito.verify(mockedObserver2).update(sut);
  }

  @Test
  void testOnRemoveDoesNotNotifyObserversIfLobbyIsNotThisLobby() {
    // Arrange
    var mockedObserver1 = Mockito.mock(IObserver.class);
    var mockedObserver2 = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver1);
    sut.observers.add(mockedObserver2);

    var mockedLobby = Mockito.mock(LobbyContract.class);
    Mockito.when(mockedLobby.getIdentifier()).thenReturn(OTHER_LOBBY_ID);

    // Act
    sut.onRemove(mockedLobby);

    // Assert
    Mockito.verify(mockedObserver1, Mockito.never()).update(sut);
    Mockito.verify(mockedObserver2, Mockito.never()).update(sut);
  }

  @Test
  void testOnStartGameCallsControllerStartGameIfLobbyIsThisLobby() {
    // Arrange

    // Act
    sut.onStartGame(mockedJoinedLobby);

    // Assert
    Mockito.verify(mockedController).startGame();
  }

  @Test
  void testOnStartGameDoesNotCallControllerStartGameIfLobbyIsNotThisLobby() {
    // Arrange
    var mockedLobby = Mockito.mock(LobbyContract.class);
    Mockito.when(mockedLobby.getIdentifier()).thenReturn(OTHER_LOBBY_ID);

    // Act
    sut.onStartGame(mockedLobby);

    // Assert
    Mockito.verify(mockedController, Mockito.never()).startGame();
  }

  @Test
  void testOnJoinNotifiesObserversIfLobbyIsThisLobby() {
    // Arrange
    var mockedObserver1 = Mockito.mock(IObserver.class);
    var mockedObserver2 = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver1);
    sut.observers.add(mockedObserver2);

    // Act
    sut.onJoin(mockedJoinedLobby);

    // Assert
    Mockito.verify(mockedObserver1).update(sut);
    Mockito.verify(mockedObserver2).update(sut);
  }

  @Test
  void testOnJoinDoesNotNotifyObserversIfLobbyIsNotThisLobby() {
    // Arrange
    var mockedObserver1 = Mockito.mock(IObserver.class);
    var mockedObserver2 = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver1);
    sut.observers.add(mockedObserver2);

    var mockedLobby = Mockito.mock(LobbyContract.class);
    Mockito.when(mockedLobby.getIdentifier()).thenReturn(OTHER_LOBBY_ID);

    // Act
    sut.onJoin(mockedLobby);

    // Assert
    Mockito.verify(mockedObserver1, Mockito.never()).update(sut);
    Mockito.verify(mockedObserver2, Mockito.never()).update(sut);
  }
}
