package nl.aimsites.nzbvuq.gameview.model;

import nl.aimsites.nzbvuq.chat.enums.ChatType;
import nl.aimsites.nzbvuq.gameview.Message;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class GameModelTest {

  private static final String MESSAGE = "MESSAGE";
  private static final String SENDER = "SENDER";
  private GameModel sut;

  @BeforeEach
  void setUp() {
    sut = GameModel.getInstance();
  }

  @Test
  void testDisplayChatMessageNotifiesObservers() {
    // Arrange
    var mockedObserver1 = Mockito.mock(IObserver.class);
    var mockedObserver2 = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver1);
    sut.observers.add(mockedObserver2);

    // Act
    sut.displayChatMessage(MESSAGE, SENDER, ChatType.GLOBAL);

    // Assert
    Mockito.verify(mockedObserver1).update(Mockito.any(Message.class));
    Mockito.verify(mockedObserver2).update(Mockito.any(Message.class));
  }

  @Test
  void testDisplayGameMessageNotifiesObservers() {
    // Arrange
    var mockedObserver1 = Mockito.mock(IObserver.class);
    var mockedObserver2 = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver1);
    sut.observers.add(mockedObserver2);

    // Act
    sut.displayGameMessage(MESSAGE);

    // Assert
    Mockito.verify(mockedObserver1).update(Mockito.any(Message.class));
    Mockito.verify(mockedObserver2).update(Mockito.any(Message.class));
  }

  @Test
  void testAddObserverAddsObserver() {
    // Arrange
    var expected = Mockito.mock(IObserver.class);

    // Act
    sut.addObserver(expected);
    var actual = sut.observers.get(0);

    // Assert
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void testRemoveObserverRemovesObserver() {
    // Arrange
    var mockedObserver = Mockito.mock(IObserver.class);
    var expected = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver);
    sut.observers.add(expected);

    // Act
    sut.removeObserver(mockedObserver);
    var actual = sut.observers.get(0);

    // Assert
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void testRemoveObserverWhenNoObserverPresent() {
    // Arrange
    var mockedObserver = Mockito.mock(IObserver.class);

    // Act & assert
    Assertions.assertDoesNotThrow(() -> sut.removeObserver(mockedObserver));

  }

  @Test
  void testNotifyObserversNotifiesObservers() {
    // Arrange
    var mockedObserver1 = Mockito.mock(IObserver.class);
    var mockedObserver2 = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver1);
    sut.observers.add(mockedObserver2);

    var mockedMessage = Mockito.mock(Message.class);

    // Act
    sut.notifyObservers(mockedMessage);

    // Assert
    Mockito.verify(mockedObserver1).update(mockedMessage);
    Mockito.verify(mockedObserver2).update(mockedMessage);
  }
}
