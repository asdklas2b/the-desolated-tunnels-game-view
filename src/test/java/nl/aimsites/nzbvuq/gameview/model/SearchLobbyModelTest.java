package nl.aimsites.nzbvuq.gameview.model;

import nl.aimsites.nzbvuq.gameview.controller.SearchLobbyController;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyManagerContract;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

public class SearchLobbyModelTest {

  private SearchLobbyModel sut;
  private LobbyManagerContract mockedLobbyManager;
  private SearchLobbyController mockedController;

  @BeforeEach
  void setUp() {
    mockedLobbyManager = Mockito.mock(LobbyManagerContract.class);

    sut = new SearchLobbyModel(mockedLobbyManager);

    mockedController = Mockito.mock(SearchLobbyController.class);
    sut.setController(mockedController);
  }

  @Test
  void testGetLobbiesCallsLobbyManagerGetLobbies() {
    // Arrange
    var lowerLobbyIndex = 1;
    var upperLobbyIndex = 4;

    var returnedLobbies = new ArrayList<LobbyContract>();
    var mockedReturnedLobby = Mockito.mock(LobbyContract.class);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);

    Mockito.when(mockedLobbyManager.getLobbies()).thenReturn(returnedLobbies);

    // Act
    sut.getLobbies(lowerLobbyIndex, upperLobbyIndex);

    // Assert
    Mockito.verify(mockedLobbyManager).getLobbies();
  }

  @Test
  void testGetLobbiesReturnsLobbyManagerGetLobbies() {
    // Arrange
    var lowerLobbyIndex = 2;
    var upperLobbyIndex = 4;

    var returnedLobbies = new ArrayList<LobbyContract>();
    var mockedReturnedLobby = Mockito.mock(LobbyContract.class);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);

    var expected = returnedLobbies.subList(lowerLobbyIndex, upperLobbyIndex);

    Mockito.when(mockedLobbyManager.getLobbies()).thenReturn(returnedLobbies);

    // Act
    var actual = sut.getLobbies(lowerLobbyIndex, upperLobbyIndex);

    // Assert
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void testGetLobbyCountCallsLobbyManagerGetLobbies() {
    // Arrange
    var returnedLobbies = new ArrayList<LobbyContract>();

    Mockito.when(mockedLobbyManager.getLobbies()).thenReturn(returnedLobbies);

    // Act
    sut.getLobbyCount();

    // Assert
    Mockito.verify(mockedLobbyManager).getLobbies();
  }

  @Test
  void testGetLobbyCountReturnsLobbyManagerGetLobbiesNLobbies() {
    // Arrange
    var returnedLobbies = new ArrayList<LobbyContract>();
    var mockedReturnedLobby = Mockito.mock(LobbyContract.class);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);

    var expected = returnedLobbies.size();

    Mockito.when(mockedLobbyManager.getLobbies()).thenReturn(returnedLobbies);

    // Act
    var actual = sut.getLobbyCount();

    // Assert
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void testGetLobbyCallsLobbyManagerGetLobbies() {
    // Arrange
    var lobbyIndex = 5;
    var returnedLobbies = new ArrayList<LobbyContract>();
    var mockedReturnedLobby = Mockito.mock(LobbyContract.class);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);

    Mockito.when(mockedLobbyManager.getLobbies()).thenReturn(returnedLobbies);

    // Act
    sut.getLobby(lobbyIndex);

    // Assert
    Mockito.verify(mockedLobbyManager).getLobbies();
  }

  @Test
  void testGetLobbyReturnsCorrectIndexOfLobbyManagerGetLobbies() {
    // Arrange
    var lobbyIndex = 5;

    var returnedLobbies = new ArrayList<LobbyContract>();
    var mockedReturnedLobby = Mockito.mock(LobbyContract.class);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);
    returnedLobbies.add(mockedReturnedLobby);

    Mockito.when(mockedLobbyManager.getLobbies()).thenReturn(returnedLobbies);

    var expected = returnedLobbies.get(lobbyIndex);

    // Act
    var actual = sut.getLobby(lobbyIndex);

    // Assert
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void testJoinLobbyCallsLobbyManagerJoinLobbyWithRightParameters() {
    // Arrange
    var mockedLobby = Mockito.mock(LobbyContract.class);

    // Act
    sut.joinLobby(mockedLobby);

    // Assert
    Mockito.verify(mockedLobbyManager).join(mockedLobby);
  }

  @Test
  void testAddObserverAddsObserver() {
    // Arrange
    var expected = Mockito.mock(IObserver.class);

    // Act
    sut.addObserver(expected);
    var actual = sut.observers.get(0);

    // Assert
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void testRemoveObserverRemovesObserver() {
    // Arrange
    var mockedObserver = Mockito.mock(IObserver.class);
    var expected = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver);
    sut.observers.add(expected);

    // Act
    sut.removeObserver(mockedObserver);
    var actual = sut.observers.get(0);

    // Assert
    Assertions.assertEquals(expected, actual);
  }

  @Test
  void testRemoveObserverWhenNoObserverPresent() {
    // Arrange
    var mockedObserver = Mockito.mock(IObserver.class);

    // Act & assert
    Assertions.assertDoesNotThrow(() -> sut.removeObserver(mockedObserver));
  }

  @Test
  void testNotifyObserversNotifiesObservers() {
    // Arrange
    var mockedObserver1 = Mockito.mock(IObserver.class);
    var mockedObserver2 = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver1);
    sut.observers.add(mockedObserver2);

    // Act
    sut.notifyObservers(sut);

    // Assert
    Mockito.verify(mockedObserver1).update(sut);
    Mockito.verify(mockedObserver2).update(sut);
  }

  @Test
  void testOnAddNotifiesObservers() {
    // Arrange
    var mockedObserver1 = Mockito.mock(IObserver.class);
    var mockedObserver2 = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver1);
    sut.observers.add(mockedObserver2);

    // Act
    sut.onAdd(Mockito.any());

    // Assert
    Mockito.verify(mockedObserver1).update(sut);
    Mockito.verify(mockedObserver2).update(sut);
  }

  @Test
  void testOnUpdateNotifiesObservers() {
    // Arrange
    var mockedObserver1 = Mockito.mock(IObserver.class);
    var mockedObserver2 = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver1);
    sut.observers.add(mockedObserver2);

    // Act
    sut.onUpdate(Mockito.any());

    // Assert
    Mockito.verify(mockedObserver1).update(sut);
    Mockito.verify(mockedObserver2).update(sut);
  }

  @Test
  void testOnRemoveNotifiesObservers() {
    // Arrange
    var mockedObserver1 = Mockito.mock(IObserver.class);
    var mockedObserver2 = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver1);
    sut.observers.add(mockedObserver2);

    // Act
    sut.onRemove(Mockito.any());

    // Assert
    Mockito.verify(mockedObserver1).update(sut);
    Mockito.verify(mockedObserver2).update(sut);
  }

  @Test
  void testOnStartGameNotifiesObservers() {
    // Arrange
    var mockedObserver1 = Mockito.mock(IObserver.class);
    var mockedObserver2 = Mockito.mock(IObserver.class);

    sut.observers.add(mockedObserver1);
    sut.observers.add(mockedObserver2);

    // Act
    sut.onStartGame(Mockito.any());

    // Assert
    Mockito.verify(mockedObserver1).update(sut);
    Mockito.verify(mockedObserver2).update(sut);
  }

  @Test
  void testOnJoinCallsControllerJoinGame() {
    // Arrange
    var mockedLobbyContract = Mockito.mock(LobbyContract.class);

    // Act
    sut.onJoin(mockedLobbyContract);

    // Assert
    Mockito.verify(mockedController).joinGame();
  }
}
