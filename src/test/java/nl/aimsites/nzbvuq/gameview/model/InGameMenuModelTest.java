package nl.aimsites.nzbvuq.gameview.model;

import nl.aimsites.nzbvuq.game.agentscriptconverter.IUseAgent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class InGameMenuModelTest {

  private InGameMenuModel sut;
  private IUseAgent mockedUseAgent;

  @BeforeEach
  void setUp() {
    mockedUseAgent = Mockito.mock(IUseAgent.class);

    sut = new InGameMenuModel(mockedUseAgent);
  }

  @Test
  void testSwapAgentCallsUseAgentStartUsingAgentIfCalledOnce() {
    // Arrange

    // Act
    sut.swapAgent();

    // Assert
    Mockito.verify(mockedUseAgent).startUsingAgent(Mockito.anyString(), Mockito.anyString());
  }

  @Test
  void testSwapAgentDoesNotCallStopUsingAgentIfCalledOnce() {
    // Arrange

    // Act
    sut.swapAgent();

    // Assert
    Mockito.verify(mockedUseAgent, Mockito.never()).stopUsingAgent(Mockito.anyString(), Mockito.anyString());
  }

  @Test
  void testSwapAgentCallsUseAgentStopUsingAgentOnceIfCalledTwice() {
    // Arrange

    // Act
    sut.swapAgent();
    sut.swapAgent();

    // Assert
    Mockito.verify(mockedUseAgent, Mockito.times(1)).stopUsingAgent(Mockito.anyString(), Mockito.anyString());
  }

  @Test
  void testSwapAgentOnlyCallsUseAgentStartUsingAgentOnceIfCalledTwice() {
    // Arrange

    // Act
    sut.swapAgent();
    sut.swapAgent();

    // Assert
    Mockito.verify(mockedUseAgent, Mockito.times(1)).startUsingAgent(Mockito.anyString(), Mockito.anyString());
  }
}
