package nl.aimsites.nzbvuq.gameview.model;

import nl.aimsites.nzbvuq.lobby.contracts.LobbyManagerContract;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class CreateLobbyModelTest {

  private CreateLobbyModel sut;
  private LobbyManagerContract lobbyManagerContract;

  @BeforeEach
  public void setup() {
    sut = new CreateLobbyModel();
    lobbyManagerContract = Mockito.mock(LobbyManagerContract.class);
    sut.setLobbyManager(lobbyManagerContract);
  }

  @Test
  void uTestCreateLobbyWithCorrectNameAndMaxAmountPlayers() {
    //Arrange
    String lobbyName = "test lobby";
    int maxAmountOfPlayers = 10;
    //Act
    sut.createLobby(lobbyName, maxAmountOfPlayers);
    //Assert
    Mockito.verify(lobbyManagerContract).create(lobbyName, maxAmountOfPlayers);
  }

  @Test
  void uTestCheckMaxAmountOfPlayersValidWhenTooHighValueGiven() {
    //Arrange
    int value = 21;
    boolean result;
    //Act
    result = sut.checkMaxAmountOfPlayersValid(value);
    //Assert
    Assertions.assertEquals(false, result);
  }

  @Test
  void uTestCheckMaxAmountOfPlayersValidWhenTooLowValueGiven() {
    //Arrange
    int value = 0;
    boolean result;
    //Act
    result = sut.checkMaxAmountOfPlayersValid(value);
    //Assert
    Assertions.assertEquals(false, result);
  }

  @Test
  void uTestCheckMaxAmountOfPlayersValidWhenCorrectValueGiven() {
    //Arrange
    int value = 18;
    boolean result;
    //Act
    result = sut.checkMaxAmountOfPlayersValid(value);
    //Assert
    Assertions.assertEquals(true, result);
  }
}
